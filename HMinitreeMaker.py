from pyLCIO import IOIMPL
from pyLCIO.io.StdHepReader import StdHepReader # to read .stdhep files
import ROOT
import collections
import time # for measuring execution time
import datetime # to convert seconds to hours:minutes:seconds
import argparse # to parse command line options
import re # to do regular expression matching
import math
import itertools

import functions.parseMetadata as parseMetadata
#import functions.fourVectorHelper as lorentzVectorWithPDGId
from functions.fourVectorHelper import lorentzVectorWithPDGId
import functions.dictToTTree as dictToTTree


# LCIO reference: https://ilcsoft.desy.de/LCIO/current/doc/doxygen_api/html/index.html
def LCIOParticleTo4VecWithPDGID(LCIOParticle, pdgId = None): 
    if pdgId is None: pdgId = LCIOParticle.getType() #make sure that getType is the correct method here
    #   remember                                             pX                             pY                             pZ                             
    return lorentzVectorWithPDGId(LCIOParticle.getMomentum()[0], LCIOParticle.getMomentum()[1], LCIOParticle.getMomentum()[2], LCIOParticle.getEnergy(), pdgId )

def LCIOParticleTo4Vec(LCIOParticle): 
    #   remember                                             pX                             pY                             pZ                         
    return lorentzVectorWithPDGId(LCIOParticle.getMomentum()[0], LCIOParticle.getMomentum()[1], LCIOParticle.getMomentum()[2], LCIOParticle.getEnergy())


def setupHistForAcceptanceCalculation(histName = "acceptanceHist"):

    acceptanceHist = ROOT.TH1I(histName,histName,2,0,2)
    #acceptanceHist.AddBinContent(1) # add 1 to n-th bin: AddBinContent(n)

    acceptanceHist.GetXaxis().SetBinLabel(1, "#Events Before Cuts")
    acceptanceHist.GetXaxis().SetBinLabel(2, "#Events After Cuts")

    return acceptanceHist

def addJetsToDict( variableDict , jetType = "Refined2Jets", higgsMassWindow = None, leptonPair = None):

    ZBosonMassGeV = 91.1876 # GeV

    def makeJet4Veclist(jetCollection):

        # turn the jets in our usual LorentzVector derived objects, so that we can use existing functions
        jet4VecList = [ LCIOParticleTo4Vec(jet) for jet in jetCollection] 
        for vec in jet4VecList: vec.setPdgId(0) 
        # set pdgIds to zero, so that we can use the 'makeLeptonPairs' function
        jet4VecList = [ LCIOParticleTo4VecWithPDGID(jet, pdgId = 0 ) for jet in jetCollection] 
        #jet4VecList = [ LCIOParticleTo4VecWithPDGID(jet, pdgId = math.copysign(1,jet.getCharge()) ) for jet in jetCollection] 

        return jet4VecList

    def getZCandicateFromJet4VecList(jet4VecList):

        orderedJetList = [] # store here the vectors in the sequence that we wanna use them. 
        # I.e. elements 0 and 1 are reconstructing the Z boson, 
        # the other four reconstrect Zds pairwise, i.e. 2,3 is a Zd, and 4,5 too

        # id the jets from the Z first, use this dict here down the line to work with the other 4 reliably
        jet4VecDict = {id(vec):vec for vec in jet4VecList} 

        ZBosonJetPairs = makeLeptonPairs(jet4VecList, sortingLambda = lambda x, refMass = ZBosonMassGeV :abs((x[0]+x[1]).M()-refMass) )

        jetPairThatReconstructsZBoson = ZBosonJetPairs[0]

        orderedJetList.extend(ZBosonJetPairs[0]) # element 0 is our best guess for the jets reconstructign the Z boson

        for vec in orderedJetList: jet4VecDict.pop( id(vec),None) # after this, jet4VecDict.values() yields the non-selected jets

        return jetPairThatReconstructsZBoson, jet4VecDict.values()


    nJetsRequested = re.search("\d", jetType).group()
    jetCollection = event.getCollection(jetType) 
    currentJetN = 0

    if nJetsRequested == "2":
        orderedJetList = [ LCIOParticleTo4VecWithPDGID(jet, pdgId = 0 ) for jet in jetCollection] 

    elif nJetsRequested == "4" and leptonPair is None: 
        jet4VecList = makeJet4Veclist(jetCollection)

        jetZPair, zdCandidateJetList = getZCandicateFromJet4VecList(jet4VecList)

        orderedJetList = [] # store here the vectors in the sequence that we wanna use them. 
        # I.e. elements 0 and 1 are reconstructing the Z boson, 
        # the other four reconstrect Zds pairwise, i.e. 2,3 is a Zd, and 4,5 too
        orderedJetList.extend(jetZPair)

        jetPairs = makeLeptonPairs(zdCandidateJetList, sortingLambda = lambda x, refMass = ZBosonMassGeV :abs((x[0]+x[1]).M()-refMass) )

        orderedJetList.extend(leptonPair[0]) # element 0 is best guess for quadruplet reconstructin ZdZd pair

    elif nJetsRequested == "4" and leptonPair is not None: 

        jet4VecList = makeJet4Veclist(jetCollection)

        ZConstituentPair, zdCandidateJetList = getZCandicateFromJet4VecList(jet4VecList)
        orderedJetList = [] # store here the vectors in the sequence that we wanna use them. 

        recoZ = ZConstituentPair[0]+ZConstituentPair[1]

        recoZFromLeps = leptonPair[0]+leptonPair[1]

        if abs(recoZ.M() - ZBosonMassGeV) < abs(recoZFromLeps.M() - ZBosonMassGeV): 
            # build the Z from jets
            buildZFromJets=True
            jetPairs = makeLeptonPairs(zdCandidateJetList, sortingLambda = lambda x, refMass = ZBosonMassGeV :abs((x[0]+x[1]).M()-recoZFromLeps.M()) )

            orderedJetList.extend(ZConstituentPair)
            orderedJetList.extend(jetPairs[0] )
            orderedJetList.extend(leptonPair)

        else: 
            # build the Z from leptons

            buildZFromJets=False

            ZConstituentPair = leptonPair

            jetPairs = makeLeptonPairs(jet4VecList, sortingLambda = lambda x, refMass = ZBosonMassGeV :abs((x[0]+x[1]).M()-refMass) )

            ZdZdQuads = quadruplets( jetPairs )

            orderedJetList.extend(leptonPair)
            orderedJetList.extend(ZdZdQuads[0])

            #for a,b,c,d in ZdZdQuads : (a+b).M()-(c+d).M()


        if higgsMassWindow is not None:
            recoHiggs = orderedJetList[2]+orderedJetList[3]+orderedJetList[4]+orderedJetList[5]

            if recoHiggs.M() < higgsMassWindow[0] or recoHiggs.M() > higgsMassWindow[1] : return False

                        # also check jet polar anble while we are at it
            for jet in orderedJetList: 
                if abs(math.cos(jet.Theta()) )>0.9 : return False


    elif nJetsRequested == "6": 

        jet4VecList = makeJet4Veclist(jetCollection)

        jetZPair, zdCandidateJetList = getZCandicateFromJet4VecList(jet4VecList)

        orderedJetList = [] # store here the vectors in the sequence that we wanna use them. 
        # I.e. elements 0 and 1 are reconstructing the Z boson, 
        # the other four reconstrect Zds pairwise, i.e. 2,3 is a Zd, and 4,5 too
        orderedJetList.extend(jetZPair)

        #ZCandidate = jetZPair[0]+jetZPair[1]

        ZdCandiatePairs = makeLeptonPairs(zdCandidateJetList)
        ZdZdQuads = quadruplets( ZdCandiatePairs )

        orderedJetList.extend(ZdZdQuads[0]) # element 0 is best guess for quadruplet reconstructin ZdZd pair


        # check higgs mass window:
        if higgsMassWindow is not None:
            recoHiggs = ZdZdQuads[0][0]+ZdZdQuads[0][1]+ZdZdQuads[0][2]+ZdZdQuads[0][3]
            if recoHiggs.M() < higgsMassWindow[0] or recoHiggs.M() > higgsMassWindow[1] : return False

            # also check jet polar anble while we are at it
            for jet in orderedJetList: 
                if abs(math.cos(jet.Theta()) )>0.9 : return False

        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    for jet in orderedJetList:

        currentJetN += 1

        branchPartName = jetType + "_%iof%s" %(currentJetN,nJetsRequested)

        variableDict[branchPartName+"_px"].append( jet.px() )
        variableDict[branchPartName+"_py"].append( jet.py() )
        variableDict[branchPartName+"_pz"].append( jet.pz() )
        variableDict[branchPartName+"_E" ].append( jet.E()      )

    return variableDict

def getAllPairs( aList ):
    nElements = len(aList)
    for item1Counter in range(0,nElements-1):
        item1 = aList[item1Counter]

        for item2Counter in range(item1Counter+1,nElements):
            item2 = aList[item2Counter]
            yield item1, item2


def makeLeptonPairs(leptonList, sortingLambda = lambda x :abs((x[0]+x[1]).M()) ):

    leptonPairList = []

    ZBosonMass = 91187.6 # MeV

    for lepton1, lepton2 in getAllPairs( leptonList ):
        if lepton1.pdgId() +  lepton2.pdgId() == 0:  
            if lepton1 is lepton2: import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
            if lepton1.pdgId() >= 0: leptonPairList.append( (lepton1,lepton2) )            
            else :                  leptonPairList.append( (lepton2,lepton1) )            

    leptonPairList.sort( key = sortingLambda , reverse=False) # 

    return leptonPairList

def getZdZdQuadruplet( fermionList ):

    fermiondQuads = [fermion for fermion in combinations(fermionList,4) ]



    return

def quadruplets( leptonPairs):

    ZBosonMass = 91187.6 # MeV
    quadrupletList = []

    for pair1, pair2 in getAllPairs( leptonPairs ):
        if (pair1[0] is pair2[0]) or (pair1[1] is pair2[1]): continue # can't reuse a lepton to make a quadruplet. Use 'is' to check against memory location

        if len( set([ id(pair1[0]), id(pair1[1]), id(pair2[0]) , id(pair2[1])])  ) <4 : continue

        massPair1 = (pair1[0] + pair1[1]).M()
        massPair2 = (pair2[0] + pair2[1]).M()

        # first two leptons should be the ones whoese invariant mass is closer to the Z-boson mass
        if abs(massPair1-ZBosonMass) <= abs(massPair2-ZBosonMass): quadrupletList.append( (pair1[0], pair1[1], pair2[0], pair2[1]) ) 
        else:                                                      quadrupletList.append( (pair2[0], pair2[1], pair1[0], pair1[1]) )

    # Sort the quadruplets
    # We put the most important sorting parameter last
    sortByDeltaMLambda = lambda x :abs((x[0]+x[1]).M()-(x[2]+x[3]).M())
    quadrupletList.sort( key = sortByDeltaMLambda , reverse=False) # 

    ## lines for inspecting the sorted quadruplets
    #if len(quadrupletList) > 3 : import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
    #for quadruplet in quadrupletList: print( quadruplet[0].pdgId(), quadruplet[1].pdgId(), quadruplet[2].pdgId(), quadruplet[3].pdgId() )
    #for quadruplet in quadrupletList: print( abs((quadruplet[0]+quadruplet[1]).M()-ZBosonMass), abs((quadruplet[2]+quadruplet[3]).M()-ZBosonMass) )

    return quadrupletList

def get4VecLeptons(event):

    leptonsLCIO = []

    for muon     in event.getCollection("IsolatedMuons"):     leptonsLCIO.append(muon)
    for electron in event.getCollection("IsolatedElectrons"): leptonsLCIO.append(electron)

    leptons = [LCIOParticleTo4VecWithPDGID(lepLCIO) for lepLCIO in leptonsLCIO]

    return leptons


def selectZdZdLeptons(event):

    leptons = get4VecLeptons(event)
    leptonPairs = makeLeptonPairs(leptons)
    quads = quadruplets( leptonPairs)

    if len(quads) < 1 : return None
    return quads[0]


def addLepsToQuadruplet(variableDict, lepQuadruplet):

    counter = 0

    for lepton in lepQuadruplet:
        counter += 1

        branchPrefix = "lepton%i" %(counter)

        variableDict[branchPrefix+"_px"].append( lepton.px() )
        variableDict[branchPrefix+"_py"].append( lepton.py() )
        variableDict[branchPrefix+"_pz"].append( lepton.pz() )
        variableDict[branchPrefix+"_E" ].append( lepton.e()      )
        variableDict[branchPrefix+"_PDGId" ].append( lepton.pdgId() )

    return None

def getMatchingEventCollection(event, collectionOptions = ["MCParticle", "MCParticlesSkimmed"] ):
    # Different samples have different names for related collections. 
    # use this function to select the collection name that is appriate for the given sample

    if not isinstance(collectionOptions, set): collectionOptions = set(collectionOptions)
    collectionsInEvent = set([collectionName for collectionName in event.getCollectionNames()])

    return list(collectionsInEvent.intersection(collectionOptions))[0]


def initilizeArgParser( parser = argparse.ArgumentParser() ):

    parser.add_argument("--input", type=str, nargs='*', help="set of paths to files which want to process")

    parser.add_argument("--metaDataFiles", type=str, nargs='*', 
        help="set of paths to metadata files that contain the event IDs for the events in the input files. \
              Use to determine the background event weights.")

    parser.add_argument("--cutflowType", type=str, choices=[ "4l_finalState", "2l2j_finalState", "4j_finalState", "4j_finalState_v2", "2l2j_finalState_v2"], required=True,
        help="Which final state of H->ZdZd -> X shall we select for?")

    parser.add_argument( "--physicsProcess", type=str, default="process",
        help = "Name of the main physics process that we are processing, e.g. ZH->jj+invisible, or background, etc"  )  

    parser.add_argument( "--nEventsToProcess", type=int, default=-1,
        help = "Number of events we want to process. --nEventsToProcess -1 implies processing of all events"  )  

    parser.add_argument( "--outputFileName", type=str, default="output_HInvCutflow.root",
        help = "Name of the main physics process that we are processing, e.g. ZH->jj+invisible, or background, etc"  )  

    return parser



if __name__ == '__main__':

    # parse command line arguments
    parser = initilizeArgParser() 
    args = parser.parse_args()

    assert len(args.input) > 0 , "No input files provided."

    # create a reader that can open LCIOfils
    reader = IOIMPL.LCFactory.getInstance().createLCReader()

    # set a number of constants
    ROOT.gROOT.SetBatch(True)
    dumpInfo = False  
    showEventNumber = False

    counter = -1 # relevant for associating event with its cross section
    startTime = time.time() 

    # allocate variables related to the cross sections
    if args.metaDataFiles is None: metaDatafiles = [None] * len(args.input)
    else                         : metaDatafiles = args.metaDataFiles
    crosssectionList = []
    processIDList = []

    outputDict = collections.defaultdict(list)  # store here information that we will output as a TTree
    acceptanceHist = setupHistForAcceptanceCalculation()

    # We allow to define multiple input files, loop over those
    for mcFile , metaDataFile in zip(args.input, metaDatafiles): 

        # use correct reader for given file type
        if mcFile.endswith("stdhep"):  reader = StdHepReader(mcFile)
        else:                          reader.open( mcFile ) # for reading .slcio files

        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
        
        # get cross section from meta data files
        if metaDataFile is None: 
            print("nEvents in %s: %i" %(mcFile,reader.getNumberOfEvents()))
            crosssectionList.extend([1] * reader.getNumberOfEvents())
            processIDList.extend([1] * reader.getNumberOfEvents())
        else:
            eventIDs = parseMetadata.get_eventID_list( metaDataFile )
            processIDList.extend(eventIDs)
            crosssectionList.extend([ parseMetadata.get_XS_for_eventID( eventID , doJSON = True) for eventID in eventIDs ])

        for event in reader:

            counter += 1 # relevant for associating event with its cross section
            acceptanceHist.AddBinContent(1)

            # select appropriate truth particle collection name for the sample file we are currently using
            if counter < 1: mcTruthCollectionName = getMatchingEventCollection(event, collectionOptions = ["MCParticle", "MCParticlesSkimmed"] )

            nPFOs = event.getCollection("PandoraPFOs").getNumberOfElements() 

            if args.cutflowType == "4l_finalState":

                jetTargetType = "Refined2Jets"
                nJetsRequired = int(re.search("\d", jetTargetType).group())

                if len(event.getCollection(jetTargetType) ) != nJetsRequired: continue

                nMuons     = event.getCollection("IsolatedMuons").getNumberOfElements()
                nElectrons = event.getCollection("IsolatedElectrons").getNumberOfElements()

                if nMuons+nElectrons < 4: continue

                lepQuadruplet = selectZdZdLeptons(event)

                if lepQuadruplet is None: continue

                addLepsToQuadruplet(outputDict, lepQuadruplet)

                outputDict["nParticleFlowObjects"].append(event.getCollection("PandoraPFOs").getNumberOfElements() )
                #outputDict["nSiTracks"].append(event.getCollection("SiTracks").getNumberOfElements() )

                addJetsToDict(outputDict , jetType = jetTargetType)

            elif args.cutflowType == "2l2j_finalState": 


                jetTargetType = "Refined4Jets"
                nJetsRequired = int(re.search("\d", jetTargetType).group())

                if len(event.getCollection(jetTargetType) ) != nJetsRequired: continue

                nMuons     = event.getCollection("IsolatedMuons").getNumberOfElements()
                nElectrons = event.getCollection("IsolatedElectrons").getNumberOfElements()

                if nMuons+nElectrons < 2: continue

                lepton4VecList = get4VecLeptons(event)

                ZBosonMassGeV = 91.1876 # GeV
                leptonPairs = makeLeptonPairs(lepton4VecList, sortingLambda = lambda x, refMass = ZBosonMassGeV :abs((x[0]+x[1]).M()-refMass) )

                if len(leptonPairs) < 1: continue

                addJetsToDict(outputDict , jetType = jetTargetType)
                addLepsToQuadruplet(outputDict, leptonPairs[0])
                


            elif args.cutflowType == "4j_finalState": 


                jetTargetType = "Refined6Jets"
                nJetsRequired = int(re.search("\d", jetTargetType).group())

                if len(event.getCollection(jetTargetType) ) != nJetsRequired: continue

                addJetsToDict(outputDict , jetType = jetTargetType)

            elif args.cutflowType == "4j_finalState_v2": 

                


                if nPFOs < 6: continue

                jetTargetType = "Refined6Jets"
                nJetsRequired = int(re.search("\d", jetTargetType).group())

                if len(event.getCollection(jetTargetType) ) != nJetsRequired: continue

                jetOK =  addJetsToDict(outputDict , jetType = jetTargetType, higgsMassWindow = (90,160))
                if not jetOK: continue
                outputDict["nPFOs"].append(nPFOs)
                outputDict["nMuons"].append(    event.getCollection("IsolatedMuons").getNumberOfElements()     )
                outputDict["nElectrons"].append(event.getCollection("IsolatedElectrons").getNumberOfElements() )
                outputDict["nSiTracks"].append(event.getCollection("SiTracks").getNumberOfElements() )

            elif args.cutflowType == "2l2j_finalState_v2": 


                jetTargetType = "Refined4Jets"
                nJetsRequired = int(re.search("\d", jetTargetType).group())

                if len(event.getCollection(jetTargetType) ) != nJetsRequired: continue

                nMuons     = event.getCollection("IsolatedMuons").getNumberOfElements()
                nElectrons = event.getCollection("IsolatedElectrons").getNumberOfElements()

                if nMuons+nElectrons < 2: continue

                lepton4VecList = get4VecLeptons(event)

                ZBosonMassGeV = 91.1876 # GeV
                leptonPairs = makeLeptonPairs(lepton4VecList, sortingLambda = lambda x, refMass = ZBosonMassGeV :abs((x[0]+x[1]).M()-refMass) )

                if len(leptonPairs) < 1: continue

                jetOK = addJetsToDict(outputDict , jetType = jetTargetType, higgsMassWindow = (90,160), leptonPair = leptonPairs[0])

                if not jetOK: continue
                addLepsToQuadruplet(outputDict, leptonPairs[0])

                outputDict["nPFOs"].append(nPFOs)
                outputDict["nMuons"].append(    event.getCollection("IsolatedMuons").getNumberOfElements()     )
                outputDict["nElectrons"].append(event.getCollection("IsolatedElectrons").getNumberOfElements() )
                outputDict["nSiTracks"].append(event.getCollection("SiTracks").getNumberOfElements() )

            acceptanceHist.AddBinContent(2)
            outputDict["weight"].append(  crosssectionList[counter] )
            outputDict["process_id"].append( processIDList[counter] )

            # Zd PDGId for the signal is 1023, if that number is part of the truth record, we assume the event is Zd signal
            outputDict["isZdSignal"].append( int(1023 in [truthParticle.getPDG() for truthParticle in event.getCollection(mcTruthCollectionName)]) )

            #particles = [truthParticle for truthParticle in event.getCollection("MCParticle") if truthParticle.getPDG() == 25]
            #particles[0].getParents()[0].getDaughters()[0].getDaughters()[0].getPDG()
            #particles = [truthParticle.getPDG() for truthParticle in event.getCollection("MCParticle")]
            #particles = [truthParticle for truthParticle in event.getCollection("MCParticle") if truthParticle.getPDG() == 22 and len(truthParticle.getDaughters()) >0]

            
            if counter == args.nEventsToProcess: break
        
    runtimeString = "Runtime: " + str(datetime.timedelta(seconds=( time.time()- startTime) )) + ", " + str(datetime.timedelta(seconds= round(float( time.time()- startTime)/counter*1000 ))) + " per 1k events "
    print(runtimeString)

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here


    ############# save output into .root file #############
    if args.outputFileName.endswith(".root"): outputFileName = args.outputFileName
    else:                                     outputFileName = args.outputFileName + ".root"

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
    if not mcFile.endswith("stdhep"): reader.close()

    dictToTTree.dictToTTree(outputDict, TFileName = outputFileName, TTreeName = "miniTree", listOfOtherRootObjectsToWriteOut = [acceptanceHist])

    print( "All done!")





