
import re
import csv
import os
import json

def get_eventID_list(fileName, col = 1, convert=int, sep=None):
    if fileName is None: return None
    with open(fileName) as fobj:
        return [convert(line.split(sep)[col]) for line in fobj]

def make_processID_XS_dict( XS_file = None):

    if XS_file is None: 
        currentFileLocation = os.path.dirname(os.path.abspath(__file__))
        XS_file = os.path.join(currentFileLocation, "background_XS_data.csv" )

    processID_XS_dict = {}

    with open(XS_file) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')

        for row in csv_reader: 

            processID_URL= row["process_id"]
            processID_reMatch = re.search(r'-?\d+\.?\d+', processID_URL)
            if not processID_reMatch: continue
            processID = int(processID_reMatch.group())

            try: crossSection = float(row['xsect'])
            except ValueError : crossSection = None

            processID_XS_dict[processID] = crossSection

    return processID_XS_dict



def make_processID_XS_dict_from_json( jsonFileName = None ):

    if jsonFileName is None: 
        currentFileLocation = os.path.dirname(os.path.abspath(__file__))
        jsonFileName = os.path.join(currentFileLocation, "genmetaByID.json" )

    with open(jsonFileName, 'r') as jsonFile:  metadaDataDict_unicode = json.load(jsonFile)

    outputDict = {}

    for processID in metadaDataDict_unicode:
        try :  crossSection = float(metadaDataDict_unicode[processID]['cross_section_in_fb'])
        except: crossSection = 0

        outputDict[int(processID)] = crossSection

    # u'230000'

    #for x in outputDict: x, outputDict[x]

    return outputDict


def get_XS_for_eventID( eventID, XS_dict = make_processID_XS_dict() ,XS_dictJSON = make_processID_XS_dict_from_json(), doJSON = False):

    if XS_dict is None: XS_dict =  make_processID_XS_dict()

    if doJSON: return XS_dictJSON.get(eventID,-1)
    else: return XS_dict.get(eventID)


if __name__ == '__main__':

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    testFileLocation = "all_SM_background_+80e-_-30e+_002.stdhep.delphes_card_DSiDi.tcl.root.txt"

    eventIDs = get_eventID_list(testFileLocation,1)
    #print(eventIDs)



    processID_XS_dict = make_processID_XS_dict()


    processID_XS_dict_JSON =  make_processID_XS_dict_from_json()


    #for x in processID_XS_dict: processID_XS_dict.get(x,0)- processID_XS_dict_JSON.get(x,0)

    #for x in processID_XS_dict:
    #    if processID_XS_dict.get(x) is not None and processID_XS_dict_JSON.get(x)is not None: 
    #        if processID_XS_dict.get(x) - processID_XS_dict_JSON.get(x) != 0: print(x, processID_XS_dict.get(x) - processID_XS_dict_JSON.get(x), processID_XS_dict.get(x) ,  processID_XS_dict_JSON.get(x))



    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
