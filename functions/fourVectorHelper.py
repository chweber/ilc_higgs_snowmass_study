import ROOT


class lorentzVectorWithPDGId(ROOT.Math.PxPyPzEVector): 
    # I need a Lorentz vector that also stores the particle Id Number, so that I can keep track pf flavor and charge
    # let's make such an object, by inheriting from ROOT.Math.PxPyPzEVector

    m_pdgId = None

    def __init__(self,px,py,pz,E, pdgId=None):
        fourVec = ROOT.Math.PxPyPzEVector(px,py,pz,E)
        # init the lorentzVectorWithPDGId with a PxPyPzEVector. Apparently necessary with newer version or ROOT or so. 
        # __init__(px,py,pz,E) causes an issue now
        super(lorentzVectorWithPDGId,self).__init__(fourVec) # I believe super(...) makes it that I inherit all of the methods from 
        self.SetE(E) # this is also to work around the energy not updating
        self.m_pdgId = pdgId

    def setPdgId(self,pdgId): self.m_pdgId = pdgId
    #def getPdgId(self,pdgId): return self.m_pdgId
    def pdgId(self): return self.m_pdgId

    def __add__(self, other): # redefine the addition operator '+'. I want to keep the pdgId under certain circumstances

        outputLorentzVec = lorentzVectorWithPDGId( self.px()+other.px(), self.py()+other.py(), self.pz()+other.pz(), self.e()+other.e())

        if self.m_pdgId == other.pdgId(): outputLorentzVec.setPdgId(self.m_pdgId)
        elif self.m_pdgId == 22 : outputLorentzVec.setPdgId(other.pdgId()) # pdgId 22 = photon
        elif other.pdgId() == 22: outputLorentzVec.setPdgId(self.m_pdgId)
        else:                     outputLorentzVec.setPdgId(0)

        return outputLorentzVec

if __name__ == '__main__':

    pass

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    #ROOT.Math.PxPyPzEVector(particle.px(), particle.py(), particle.pz() , particle.e())
    #ROOT.Math.PxPyPzEVector(particle.px(), particle.py(), particle.pz() , particle.e())


 
    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
