import ROOT
from array import array # to fill the TTree eventally


def dictToTTree(outputDict, TFileName = "miniTree.root", TTreeName = "miniTree", listOfOtherRootObjectsToWriteOut = None):

    if len(outputDict) == 0: return None

    tFile = ROOT.TFile( TFileName, 'recreate')   # https://wiki.physik.uzh.ch/cms/root:pyroot_ttree
    TTree = ROOT.TTree( TTreeName, TFileName)

    # We need to store the entries for each branch temporarily in an array before writing to the TTree. 
    arrayDict = {} # use this dict for that
    for variable in outputDict: 
        if isinstance(outputDict[variable][0], int ):
            arrayDict[variable] = array('i',[0]);      
            TTree.Branch(variable  ,    arrayDict[variable]  , variable + '/I')

        else: # float
            arrayDict[variable] = array('f',[0]);      
            TTree.Branch(variable  ,    arrayDict[variable]  , variable + '/F')

    nEntries = len(list(outputDict.values())[0])

    for entryNr in range(0,nEntries): 
        for variable in outputDict: arrayDict[variable][0] = outputDict[variable][entryNr]
        TTree.Fill()


    if listOfOtherRootObjectsToWriteOut is not None:
        for rootObject in listOfOtherRootObjectsToWriteOut: rootObject.Write()

    tFile.Write()
    tFile.Close()

    return None

if __name__ == '__main__':

    pass

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    #ROOT.Math.PxPyPzEVector(particle.px(), particle.py(), particle.pz() , particle.e())
    #ROOT.Math.PxPyPzEVector(particle.px(), particle.py(), particle.pz() , particle.e())


 
    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
