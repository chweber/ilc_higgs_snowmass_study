from pyLCIO import IOIMPL
import ROOT
import collections
import time # for measuring execution time
import datetime # to convert seconds to hours:minutes:seconds


#import numpy as np


# LCIO reference: https://ilcsoft.desy.de/LCIO/current/doc/doxygen_api/html/index.html

def dumpEventContent(aEvent):

        print 'Content of event number %s' % ( aEvent.getEventNumber() )
        # loop over all the collections in the event
        for collectionName, collection in aEvent: 
            print '\t%s of type %s with %d elements' % ( collectionName, collection.getTypeName(), collection.getNumberOfElements() )

        return None


def LCIOParticleTo4Vec(LCIOParticle): 
    #   remember                                             pX                             pY                             pZ
    return ROOT.Math.PxPyPzEVector(LCIOParticle.getMomentum()[0], LCIOParticle.getMomentum()[1], LCIOParticle.getMomentum()[2], LCIOParticle.getEnergy() )

def cutflow(event, cutflowDict):

    cutflowDict["No Cut"] +=1

    # zero isolated leptons

    nMuons = event.getCollection("IsolatedMuons").getNumberOfElements()
    nElectrons = nMuons = event.getCollection("IsolatedElectrons").getNumberOfElements()

    if nMuons+nElectrons > 0 : return False
    cutflowDict["nLepton == 0"] +=1


    # reference https://arxiv.org/abs/2002.12048 mentions a 'pre-cut'
    # but it is not clear to me what it is. So Let's skip that one for now

    nParticleFlowObjects = event.getCollection("PandoraPFOs").getNumberOfElements() 

    nTracks = event.getCollection("SiTracks").getNumberOfElements() 

    if nParticleFlowObjects <= 15 and nTracks <= 6: return False
    cutflowDict["nPFO > 15 & nTrack > 6"] +=1

    jetCollection = event.getCollection("Refined2Jets") 

    jet4Vectors = [ LCIOParticleTo4Vec(jet) for jet in jetCollection ]

    recoZ4Vector = jet4Vectors[0]+jet4Vectors[1] # 4 vector ofthe Z boson, reconstructed from our two jets


    # reconstructed Z-boson pT cut
    if recoZ4Vector.pt() < 20 or recoZ4Vector.pt() > 80: return False # I presume the units here are GeV
    cutflowDict["pTjj cut"] +=1

    # reconstructed Z-boson mass cut
    if recoZ4Vector.M() < 80 or recoZ4Vector.M() > 100 : return False
    cutflowDict["mjj cut"] +=1

    # polar angle cut: recoZ4Vector.Theta() - polar angle

    if abs( ROOT.TMath.Cos( recoZ4Vector.Theta() ) ) > 0.9 : return False
    cutflowDict["polar angle cut"] +=1

    ILC_CenterOfMassEnergy = 250 # GeV

    # reconstruc the particle against which the Z-boson recoils
    recoil4Vec = ROOT.Math.PxPyPzEVector( -recoZ4Vector.px(), -recoZ4Vector.px(), -recoZ4Vector.px(), ILC_CenterOfMassEnergy-recoZ4Vector.E() )

    # recoil mass cut
    if recoil4Vec.M() < 100 or recoil4Vec.M() > 160 : return False
    cutflowDict["m recoil"] +=1

    fourVectorDict = { "recoZ4Vector" : recoZ4Vector, "recoil4Vec" : recoil4Vec  }

    return fourVectorDict


def getTruthParticleByPDGID(mcParticleCollection, pdgId):
    # event.getCollection("MCParticle")
    return [ particle for particle in mcParticleCollection if particle.getPDG() == pdgId ]


def getCutflowYieldHist(cutflowYieldDict):

    yieldList = [ (cutflowYieldDict[key], key) for key in cutflowYieldDict]

    yieldList.sort( key = lambda x:x[0], reverse=True) 

    yieldHist = ROOT.TH1D("YieldHist", "Cutflow yield", len(yieldList), 0 - 0.5, len(yieldList)-0.5)

    for binNr in xrange( len(yieldList) ): yieldHist.SetBinContent(binNr, float(yieldList[binNr][0]) / yieldList[0][0] )

    return yieldHist

if __name__ == '__main__':

    # create a reader and open an LCIO file
    reader = IOIMPL.LCFactory.getInstance().createLCReader()

    #mcFile = "snowmass_ILC_MC/analysis-walkthrough/signal/miniDST-delphes/eeZH_m10_LR.delphes.slcio"
    #mcFile = "delphes/2f1h_inv/ilc250_eLpR_2f1hinv.1.whizard_2_6_4.stdhep.delphes_card_DSiDi.tcl.root"
    #mcFile = "geant4/2f1h_inv/ilc250_eLpR_2f1hinv.1.whizard_2_6_4.stdhep.ilcsoft_v02-00-02.sid_o2_v03.slcio"
    #mcFile = "SiDRecoOut.slcio"
    #mcFile = "whizard/2f1h_inv/ilc250_eLpR_2f1hinv.1.whizard_2_6_4.stdhep"

    #mcFile = "geant4/siddbd2013_geant4_ILC250_all_SM_background/all_SM_background_+80e-_-30e+_000.stdhep.sim.v02-00-02.SiD_o2_v03.slcio"
    #mcFile = "ilcsnowmass.com/SGV-mini-DST-E250-SetA.Pe1e1h.Gwhizard-2_8_4.eL.pR.I402001_SGVDST.0.slcio"


    mcFile = "UOregonSamples/ilc250_eLpR_2f1hinv.1.whizard_2_6_4.stdhep.ilcsoft_v02-00-02.sid_o2_v03_reco.slcio"
    ### for accessing the stdhep files:
    # from pyLCIO . io . StdHepReader import StdHepReader
    #reader = StdHepReader.StdHepReader( "whizard/2f1h_inv/ilc250_eLpR_2f1hinv.1.whizard_2_6_4.stdhep") 
    ## https://agenda.linearcollider.org/event/6933/contributions/34099/attachments/28124/42567/calancha_asian_meeting_2015-11-27.pdf
    ## https://agenda.linearcollider.org/event/5840/contributions/26280/attachments/21720/34050/pyLCIO.pdf


    # object_methods = [method_name for method_name in dir(object)  if callable(getattr(object, method_name))]
    # object_methods = [method_name for method_name in dir(reader)  if callable(getattr(reader, method_name))]
    # object_methods = [method_name for method_name in dir(event)  if callable(getattr(event, method_name))]
    #object_methods = [method_name for method_name in dir(eventParameters)  if callable(getattr(eventParameters, method_name))]


    #from pyLCIO import LCTOOLS
    #UTIL.LCTOOLS.dumpEvent(event)

    reader.open( mcFile )

    dumpInfo = False  
    showEventNumber = False


    cutflowYieldDict = collections.defaultdict(int)

    startTime = time.time() 


    # loop over all events in the file

    nBins = 50 # number of bins
    histMinimum = 0
    histMaximum = -1 # set meximum below minimum to trigger automatic binning, usefull when we don't know what our maximum will be

    recoilPTHist = ROOT.TH1D("recoilPT","recoilPT", nBins,  histMinimum, histMaximum)
    recoilPTHist.GetXaxis().SetTitle("pT (GeV)")

    truthHiggsPTHist = recoilPTHist.Clone("truthHiggsPTHist"); truthHiggsPTHist.SetName("HiggsPT")
    jjPTHist = recoilPTHist.Clone("jjPTHist"); truthHiggsPTHist.SetName("jjPT")

    recoilMassHist = ROOT.TH1D("recoilMass","recoilMass", nBins,  histMinimum, histMaximum)
    recoilMassHist.GetXaxis().SetTitle("m (GeV)")
    recoilMassHist.SetStats( False) # remove stats box
    truthHiggsMassHist = recoilMassHist.Clone("truthHiggsMassHist"); truthHiggsMassHist.SetName("HiggsMass")
    jjMassHist = recoilMassHist.Clone("jjMassHist"); jjMassHist.SetName("jjMass")

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    counter = 0
    for event in reader:

        if dumpInfo and event.getEventNumber() ==0: dumpEventContent(event)
        if showEventNumber: print( "event number %i" %event.getEventNumber()  )


        cutflowOutDict = cutflow(event, cutflowYieldDict)

        truthHiggs = getTruthParticleByPDGID(event.getCollection("MCParticle"), 25)[0] # pdgId 25 -> Higgs

        
        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
        
        if cutflowOutDict: 
            recoilMassHist.Fill(cutflowOutDict["recoil4Vec"].M())
            jjMassHist.Fill(cutflowOutDict["recoZ4Vector"].M())

        truthHiggsMassHist.Fill(truthHiggs.getMass() )


        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

        counter += 1
        #if counter > 100: break
        



    runtimeString = "Runtime: " + str(datetime.timedelta(seconds=( time.time()- startTime) ))
    print(runtimeString)

    cutflowYieldhist = getCutflowYieldHist(cutflowYieldDict)


    outoutROOTFile = ROOT.TFile("HInvCutflowOut"+".root","RECREATE")

    recoilMassHist.Write() # write to the .ROOT file
    jjMassHist.Write() # write to the .ROOT file
    truthHiggsMassHist.Write() # write to the .ROOT file
    cutflowYieldhist.Write() # write to the .ROOT file

    outoutROOTFile.Close()


    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
    #nElectrons = event.getCollection("IsolatedElectrons").getNumberOfElements()
    #nMuons = event.getCollection("IsolatedMuons").getNumberOfElements()

    #if nElectrons >0 or nMuons >0: continue


    #event.getCollection("Durham2Jets")

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
    reader.close()




    import pdb; pdb.set_trace() # import the debugger and instruct it to stop here


# less try to access some extra event info
# taken from: https://atlaswww.hep.anl.gov/hepsim/doc/doku.php?id=hepsim:usage_full

# from pyLCIO import EVENT
# 
# eventParameters = event.getParameters()
# 
# params = eventParameters
# 
# 
# floatKeys  = EVENT.StringVec()
# 
# params.getFloatKeys( floatKeys)
# 
# eventParameters.getFloatKeys(floatKeys).size()
# eventParameters.getIntKeys(floatKeys).size()
# eventParameters.getStringKeys(floatKeys).size()


