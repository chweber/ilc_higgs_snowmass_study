TMVAClassification_Danyi.C
Version from Danyi taken from here:
https://gitlab.cern.ch/dazhang/ILC_Higgs_Snowmass_Study_TMVA/-/blob/master/TMVAClassification.C


TMVAClassification.C
Likely the original, I found it here:
https://github.com/root-project/root/blob/master/tutorials/tmva/TMVAClassification.C

Instriction for this example can be found here:
https://root.cern/doc/master/TMVAClassification_8C.html

And this looks like it could the default input for the original: tmva_class_example.root
I found it here:
https://github.com/iml-wg/tutorial_UNAM/blob/master/tmva_class_example.root