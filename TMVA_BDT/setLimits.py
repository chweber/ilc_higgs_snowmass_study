import ROOT
import copy
import collections
import functions.tGraphHelpers as graphHelper
import os


def prepMeasurement( templatePaths, mass, doStatError = False, doBranchingRatioLimit = False):

    # stuff for branching ratio calculations
    XS_H = 211. #[fb]

    BR_Zd_ll = {20 : .300, 40: .278 , 60: .214 }
    BR_Zd_jj = {20 : .563, 40: .594 , 60: .657 }

    ### Create the measurement object ### This is the top node of the structure  ### We do some minor configuration as well
    meas = ROOT.RooStats.HistFactory.Measurement("ZXMeasurement", "ZXMeasurement")

    ### Set the prefix that will appear before all output for this measurement We Set ExportOnly to false, meaning we will fit the measurement and make  plots in addition to saving the workspace
    meas.SetOutputFilePrefix("./testHistfactoryOutput/")
    meas.SetExportOnly(False)

    ### Set the name of the parameter of interest Note that this parameter hasn't yet been created, we are anticipating it
    meas.SetPOI("SigXsecOverSM")

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    #meas.AddConstantParam("Lumi")           # this is not part of the C++ exsample
    #meas.AddConstantParam("alpha_syst1")    # this is not part of the C++ exsample

    ### Set the luminosity There are a few conventions for this. Here, we assume that all histograms have already been scaled by luminosity
    meas.SetLumi(1.0)
    #meas.SetLumi(1150.0)

    # Create a channel


    for finalState in templatePaths: 



        if   "4j"   in finalState :  BR_ZdZd_FS = BR_Zd_jj[mass] * BR_Zd_jj[mass] ; finalStateShort = "4j"
        elif "2l2j" in finalState :  BR_ZdZd_FS = BR_Zd_ll[mass] * BR_Zd_jj[mass] ; finalStateShort = "2l2j"


        BR_scaler = XS_H * BR_ZdZd_FS



        ### Okay, now that we've configured the measurement, we'll start building the tree. We begin by creating the first channel
        chan = ROOT.RooStats.HistFactory.Channel(finalState)
        ### First, we set the 'data' for this channel The data is a histogram represeting the measured distribution.  
        # It can have 1 or many bins. In this example, we assume that the data histogram is already made and saved in a ROOT file.   
        # So, to 'set the data', we give this channel the path to that ROOT file and the name of the data histogram in that root file 
        # The arguments are: SetData(HistogramName, HistogramFile)
        #chan.SetData(templatePaths["background"] )   # <- this seems to work, everything seems to run ok, but the programm completeres with a segmentation violation.
        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
        chan.SetData(templatePaths[finalState]["background"], templatePaths[finalState]["TFile"]) # <- this one compleres without a segmentation vialation. Switch to this one if necessary

        #chan.SetStatErrorConfig(0.05, "Poisson") # I cant discern what this does. So let's not use it. It was also not in the reference I used

        # Now set the signla template

        # Create the signal sample now that we have a channel and have attached data to it, 
        signal = ROOT.RooStats.HistFactory.Sample("signal", templatePaths[finalState]["signal"], templatePaths[finalState]["TFile"])

        ### Having created this sample, we configure it First, 
        #we add the cross-section scaling parameter that we call SigXsecOverSM 
        signal.AddNormFactor("SigXsecOverSM", 1, 0, 10) #  (<parameterName>, <start>, <lowLimit>, <highLimit>) keep the lower limit here at 0, otherwise the norm factor may get negative, which will introduce errors in the optimization routine
        if doBranchingRatioLimit: signal.AddNormFactor("scaleToBR_"+finalStateShort, BR_scaler , BR_scaler, BR_scaler,True)
        if doStatError: signal.ActivateStatError()

        #we add it to our channel
        chan.AddSample(signal)

        # Background, we only use 1 background, but could add multiple if we wanted to
        ### And we create a second background for good measure
        background = ROOT.RooStats.HistFactory.Sample("background",templatePaths[finalState]["background"] , templatePaths[finalState]["TFile"])
        if doStatError: background.ActivateStatError()

        chan.AddSample(background)


        # Done with this channel
        # Add it to the measurement:
        ### Now that we have fully configured our channel, we add it to the main measurement
        meas.AddChannel(chan)
        print( "Added the \'%s\' channel to measurement" %finalState)

    # Collect the histograms from their files,
    # print some output,
    ### At this point, we have only given our channel and measurement the input histograms as strings 
    # We must now have the measurement open the files, collect the histograms, copy and store them. This step involves I/O 
    meas.CollectHistograms()

    ### Print to the screen a text representation of the model just for minor debugging
    #meas.PrintTree();

    # One can print XML code to an output directory:
    # meas.PrintXML("xmlFromCCode", meas.GetOutputFilePrefix());

    meas.PrintXML("tutorialBuildingHistFactoryModel", meas.GetOutputFilePrefix());

    #meas.CollectHistograms()
    chan.CollectHistograms() #  see here why this is needed: https://root-forum.cern.ch/t/histfactory-issue-with-makesinglechannelmodel/34201

    return meas

def translateLimits( rooStatsObject, nSigmas = 1 , getObservedAsymptotic = False):
    # we assume that there is always only one parameter of interest
    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    if isinstance( rooStatsObject , ROOT.RooStats.LikelihoodInterval ):
        limitObject = TDirTools.rooArgSetToList( rooStatsObject.GetBestFitParameters() )[0]
        
        bestEstimate = limitObject.getVal()
        lowLimit  = rooStatsObject.LowerLimit( limitObject )
        highLimit = rooStatsObject.UpperLimit( limitObject )

        suffix = "ProfileLikelihoodObserved"

    elif isinstance( rooStatsObject , ROOT.RooStats.HypoTestInverterResult ):
        limitObject = rooStatsObject

        if getObservedAsymptotic:

            bestEstimate = rooStatsObject.UpperLimit()

            lowLimit  = rooStatsObject.UpperLimit()
            highLimit = rooStatsObject.UpperLimit()

            suffix = "observedUpperLimitAsymptotic"

        else: 

            bestEstimate = limitObject.GetExpectedUpperLimit(0)

            lowLimit  = rooStatsObject.GetExpectedUpperLimit(-nSigmas)
            highLimit = rooStatsObject.GetExpectedUpperLimit(+nSigmas)

            # limitObject.UpperLimit() observed upper limit for HypoTestInverterResult

            suffix = "expectedUpperLimitAsymptotic"

    name = limitObject.GetName() +"_"+str(nSigmas) +"SigmaLimit" + "_" + suffix
    title = limitObject.GetTitle() +"_"+str(nSigmas) +"SigmaLimit" + "_" + suffix

    outputRooRealvar = ROOT.RooRealVar( name, title ,bestEstimate,lowLimit , highLimit)
    # get the limits via .getVal(), .getMin(), .getMax()

    return outputRooRealvar



def prepAsymptoticCalculator(workspace ):

    modelConfig = workspace.obj("ModelConfig") # modelConfig = modelConfig
    data = workspace.data("obsData")

    # setup the cloned modelConfig
    modelConfigClone = modelConfig.Clone( modelConfig.GetName()+"Clone" )
    mcClonePOI = modelConfigClone.GetParametersOfInterest().first()

    mcClonePOI.setVal(1.0)
    modelConfigClone.SetSnapshot( ROOT.RooArgSet( mcClonePOI ) )

    #setup the background only model

    bModel = modelConfig.Clone("BackgroundOnlyModel")
    bModelPOI = bModel.GetParametersOfInterest().first()

    bModelPOI.setVal(0)
    bModel.SetSnapshot( ROOT.RooArgSet( bModelPOI )  )

    #  AsymptoticCalculator(data, alternativeModel, nullModel)
    asympCalc = ROOT.RooStats.AsymptoticCalculator(data, bModel, modelConfigClone ) # asymptotic calculator is for the profile likelihood ratio
    #asympCalc.SetOneSided(True);
    asympCalc.SetPrintLevel(0) # suppress command line output 

    keepInScopeList = [modelConfig, data, modelConfigClone, mcClonePOI, bModel, bModelPOI]

    return asympCalc , keepInScopeList

def writeOutWorkspaces(workspace, masspoint , outputDir = None, doBranchingRatioLimit = False):

    workspace.SetName("combined") # rename so that the workspace conforms with the expectation of StandardHypoTestInv at 
    # https://gitlab.cern.ch/atlas_higgs_combination/software/StandardHypoTestInv 

    modelConfig = workspace.obj("ModelConfig") # modelConfig = modelConfig

    #ROOT.RooStats.SetAllConstant( modelConfigClone.GetNuisanceParameters() );
    mcPOI = modelConfig.GetParametersOfInterest().first()
    mcPOI.setVal(1.0)
    modelConfig.SetSnapshot( ROOT.RooArgSet( mcPOI ) )

    if outputDir is None:  outputDir = "workspaces"

    if not os.path.exists(outputDir): os.mkdir(outputDir)


    # tally included datasets, so we can print them out
    includedDataSets = []
    for dataSet in workspace.allData(): includedDataSets.append(dataSet.GetName())


    outputFileName = "ZdZd_XS_Workspace_massTargetTrained_mZd_%iGeV.root"%( masspoint )

    if doBranchingRatioLimit: outputFileName = "ZdZd_BR_Workspace_massTargetTrained_mZd_%iGeV.root"%( masspoint )


    outTFile = ROOT.TFile( os.path.join(outputDir,outputFileName) , "RECREATE")
    workspace.Write()
    outTFile.Close()

    print("workspace '%s' written to '%s'. Included datasets: %s" %(workspace.GetName(), os.path.join(outputDir,outputFileName), ", ".join(includedDataSets)) )

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    return None


def expectedLimitsAsimov(workspace, confidenceLevel = 0.95, drawLimitPlot = False ):
    # get expected upper limits on the parameter of interest using the 'AsymptoticCalculator'
    # provides also +/- n sigma intervals on the expected limits
    # I don't understand this 'AsymptoticCalculator' fully yet, but the expected limits look reasonable 
    # I based this here on the following tutorial: https://roostatsworkbook.readthedocs.io/en/latest/docs-cls.html#

    asympCalc, keepInScopeList = prepAsymptoticCalculator( workspace )
    asympCalc.SetOneSided(True);

    inverter = ROOT.RooStats.HypoTestInverter(asympCalc)
    inverter.SetConfidenceLevel( confidenceLevel );
    inverter.UseCLs(True);
    inverter.SetVerbose(False);
    inverter.SetFixedScan(60,0.0,6.0); # set number of points , xmin and xmax

    result =  inverter.GetInterval();
    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    if drawLimitPlot: 
        hypoCanvas = ROOT.TCanvas("hypoCanvas", "hypoCanvas", 1300/2,1300/2)
        inverterPlot = ROOT.RooStats.HypoTestInverterPlot("HTI_Result_Plot","HypoTest Scan Result",result);
        inverterPlot.Draw("CLb 2CL");  # plot also CLb and CLs+b
        hypoCanvas.Update()

        import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    return result


if __name__ == '__main__':

    # setup a dict of dict that we use to store the locations of the relevant .root files,
    # and the name of the signal and background histrograms that go into the limit setting
    templatePaths = collections.defaultdict(lambda: collections.defaultdict(dict)) 

    doBranchingRatioLimit = False

    observedLimitGraph    = graphHelper.createNamedTGraphAsymmErrors("observedLimitGraph")
    expectedLimitsGraph_1Sigma = graphHelper.createNamedTGraphAsymmErrors("expectedLimits_1Sigma")
    expectedLimitsGraph_2Sigma = graphHelper.createNamedTGraphAsymmErrors("expectedLimits_2Sigma")

    # prepare the locations, paths, and names of the relevant template histograms
    for mass in [20,40,60]:

        templatePaths["finalSate_4j"]["signal"] = "averageMZd_signal_%iGeV" %mass
        templatePaths["finalSate_4j"]["background"] = "averageMZd_H_ZZ_Background"
        templatePaths["finalSate_4j"]["TFile"] = "minitreesV3_post_BDT_hists/BDTHistOut_4j_%iGeV_trained_HZZ.root" %mass
        #templatePaths["finalSate_4j"]["TFile"] = "minitreesV3_post_BDT_hists/BDTHistOut_4j_204060GeV_trained_HZZ.root" 


        templatePaths["finalSate_2l2j"]["signal"] = "averageMZd_signal_%iGeV" %mass
        templatePaths["finalSate_2l2j"]["background"] = "averageMZd_H_ZZ_Background"
        templatePaths["finalSate_2l2j"]["TFile"] = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_%iGeV_trained_HZZ.root" %mass
        #templatePaths["finalSate_2l2j"]["TFile"] = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_204060GeV_trained_HZZ.root" 

        # setup the 'measurement' object that we use to make a histfactory workspace
        meas = prepMeasurement( templatePaths, mass , doStatError = False , doBranchingRatioLimit = doBranchingRatioLimit)

        hist2workspace = ROOT.RooStats.HistFactory.HistoToWorkspaceFactoryFast(meas)


        ##### Do single channel limits ####
        ##channelName = list(templatePaths.keys())[0]
        #channelName = "finalSate_4j"
        #chan = meas.GetChannel()
        ##One can also create a workspace for only a single channel of a model by supplying that channel:
        ## see here for an example:  https://www.nikhef.nl/~vcroft/KaggleFit-Histfactory.html
        #workspace = hist2workspace.MakeSingleChannelModel(meas, chan)

        # we have multiple final states, let's setup a workspace combining the different final states
        workspace = hist2workspace.MakeCombinedModel(meas)


        # write out the workspaces, we don't use that here, but it might be usefull for other applications
        # for example if we want to calculate toy based limits on the grid
        writeOutWorkspaces(workspace,mass, doBranchingRatioLimit = doBranchingRatioLimit)

        # calculate limits here. In this case we do not use toys, but instead an asympotic formulation of the limit process
        asymptoticResuls = expectedLimitsAsimov( workspace , drawLimitPlot = False)


        likelihoodLimit = translateLimits(asymptoticResuls, nSigmas = 1)
        likelihoodLimit_2Sig = translateLimits(asymptoticResuls, nSigmas = 2)

        likelihoodLimitObserved = translateLimits( asymptoticResuls, nSigmas = 1 , getObservedAsymptotic = True)


        print("observedLimit %s"    %likelihoodLimitObserved.getVal() )

        print("expectedLimit %s"    %likelihoodLimit.getVal() )

        
        print("upperLimits1Sig %s fb"  %likelihoodLimit.getMax()         )
        print("upperLimits2Sig %s fb"  %likelihoodLimit_2Sig.getMax()    )
        print("lowLimits1Sig  %s fb"   %likelihoodLimit.getMin()         )
        print("lowLimits2Sig  %s fb"   %likelihoodLimit_2Sig.getMin()    )
        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

        graphHelper.fillTGraphWithRooRealVar(observedLimitGraph, mass, likelihoodLimitObserved)
        graphHelper.fillTGraphWithRooRealVar(expectedLimitsGraph_1Sigma, mass, likelihoodLimit)
        graphHelper.fillTGraphWithRooRealVar(expectedLimitsGraph_2Sigma, mass, likelihoodLimit_2Sig)


        BR_H_ZdZd_2l2j = likelihoodLimit.getVal()/262.


        BR_Zd_2l = 0.26
        BR_Zd_2j =  0.06 + 0.20 + 0.32  # BR(Zd -> bb + cc + light flavor)




        BR_H_ZdZd = BR_H_ZdZd_2l2j / BR_Zd_2l / BR_Zd_2j

        print("BR H->ZdZd->2l2j = %f" %BR_H_ZdZd_2l2j)
        print("BR H->ZdZd  = %f" %BR_H_ZdZd)


    writeTFile = ROOT.TFile( "combinedZdZdLimits.root",  "RECREATE")# "UPDATE")
    writeTFile.cd()

    observedLimitGraph.Write()
    expectedLimitsGraph_1Sigma.Write()
    expectedLimitsGraph_2Sigma.Write()

    writeTFile.Close()

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here