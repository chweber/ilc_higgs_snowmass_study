import ROOT
import os
import collections
import re
import copy
import itertools
import time # for measuring execution time
import datetime # to convert seconds to hours:minutes:seconds
import math

# import sys and os.path to be able to import things from the parent directory
import sys 
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) ) # need to append the parent directory here explicitly to be able to import plotPostProcess


#sys.path.append( path.dirname( path.abspath(__file__) ) )  # need to append the parent directory here explicitly to be able to import plotPostProcess

sys.path.append( os.path.join(path.dirname( path.dirname( path.abspath(__file__) ) ) , "functions") )
import dictToTTree as dictToTTree 

import weightHMinitree

from  weightHMinitree import readMinitreeToDictFromRootFile
#ROOT.Math.PxPyPzEVector(px,py,pz,E)


def makeFourVectorFromDictRootDict(rootDict, particlePrefix, index):

    px = rootDict[particlePrefix +"_px"][index]
    py = rootDict[particlePrefix +"_py"][index]
    pz = rootDict[particlePrefix +"_pz"][index]
    E  = rootDict[particlePrefix +"_E"][index]

    #ROOT.Math.PxPyPzEVector(px,py,pz,E)

    return ROOT.Math.PxPyPzEVector(px,py,pz,E)

def getDeltaR(particle1, particle2): return ( (particle1.eta() - particle2.eta())**2 + (particle1.phi() - particle2.phi())**2)**0.5


def getVectorSum(*inVectors):
    outVector = ROOT.Math.PxPyPzEVector(0,0,0,0)
    for vector in inVectors: outVector += vector
    return outVector


def makeTMVAInputVariables(rootDict , fermions ):


    nFermions = len(fermions)
    referenceTime =time.time()

    fourdiFermionName = "ffff2345"

    for rootDictIndex in range(0,len(list(rootDict.values())[0])):
        if rootDictIndex%1000 == 1: referenceTime = time.time()

        
        diJetInvarientMasses = []

        diFermion4VectorDict = {}
        
        for fermionIndex in range(0,nFermions, 2): 

            vec1 = makeFourVectorFromDictRootDict(rootDict, fermions[fermionIndex  ], rootDictIndex)
            vec2 = makeFourVectorFromDictRootDict(rootDict, fermions[fermionIndex+1], rootDictIndex)

            diFermion4Vector = getVectorSum(vec1,vec2)
            diFermionName = "ff%i%i" %(fermionIndex,fermionIndex+1)

            diFermion4VectorDict[diFermionName] = diFermion4Vector

            rootDict[diFermionName +"_pt"].append(diFermion4Vector.pt())
            rootDict[diFermionName +"_M"].append(diFermion4Vector.M())
            rootDict[diFermionName +"_E"].append(diFermion4Vector.E())
            rootDict[diFermionName +"_CosTheta"].append( math.cos( diFermion4Vector.Theta() ))

            
        # record deltaR between di-lepton four vectors, i.e. deltaR between the different bosons
        for bosonPairs in itertools.combinations(diFermion4VectorDict.keys(), 2): 

            bosonA = diFermion4VectorDict[bosonPairs[0]]
            bosonB = diFermion4VectorDict[bosonPairs[1]]

            rootDict["dR_%s_%s" %(bosonPairs[0],bosonPairs[1])].append( getDeltaR(bosonA, bosonB) )


        higgsFermions = [makeFourVectorFromDictRootDict(rootDict, fermions[indx  ], rootDictIndex) for indx in range(2,nFermions) ]
        fourFermionVector = getVectorSum( *higgsFermions ) # use the '*' to unpack the list for the function call

        
        rootDict[fourdiFermionName +"_pt"].append(fourFermionVector.pt())
        rootDict[fourdiFermionName +"_M"].append(fourFermionVector.M())
        rootDict[fourdiFermionName +"_E"].append(fourFermionVector.E())

        if rootDictIndex%1000 == 0 : print( "working, %i events processed in %s s" %(rootDictIndex, str(datetime.timedelta(seconds=( time.time() - referenceTime)) )) )

        #allFermions = [makeFourVectorFromDictRootDict(rootDict, fermions[indx  ], rootDictIndex) for indx in range(0,nFermions) ]

    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
    print(rootDict['weight'][0])

    return None

if __name__ == '__main__':


    #rootDict = readMinitreeToDictFromRootFile(ROOTFileLocation, TTreeName)

    TFileNameList = ["minitreesV3_weighted/minitreeOut_zd60_4j_weighted.root",
                     "minitreesV3_weighted/minitreeOut_zd40_4j_weighted.root",
                     "minitreesV3_weighted/minitreeOut_zd20_4j_weighted.root",
                     "minitreesV3_weighted/minitreeOut_zd60_2l2j_weighted.root",
                     "minitreesV3_weighted/minitreeOut_zd40_2l2j_weighted.root",
                     "minitreesV3_weighted/minitreeOut_zd20_2l2j_weighted.root"]
                     #"minitreesV3_weighted/minitreeOut_UOregonBackgrounds1XrecoeLpR_4j_weighted.root",
                     #"minitreesV3_weighted/minitreeOut_UOregonBackgrounds1XrecoeRpL_4j_weighted.root",
                     #"minitreesV3_weighted/minitreeOut_UOregonBackgrounds1XrecoeLpR_2l2j_weighted.root",
                     #"minitreesV3_weighted/minitreeOut_UOregonBackgrounds1XrecoeRpL_2l2j_weighted.root",
                     #"minitreesV3_weighted/minitreeOut_2f1hrecoeLpR_4j_weighted.root",
                     #"minitreesV3_weighted/minitreeOut_2f1hrecoeLpR_2l2j_weighted.root",
                     #"minitreesV3_weighted/minitreeOut_2f1hrecoeRpL_4j_weighted.root",
                     #"minitreesV3_weighted/minitreeOut_2f1hrecoeRpL_2l2j_weighted.root"]

    #TFileNameList = ["minitreesV3_weighted/minitreeOut_4fZZrecoeLpR_4j_weighted.root  ","minitreesV3_weighted/minitreeOut_4fZZrecoeLpR_2l2j_weighted.root"]



    #TFileName = "minitreesV2_weighted/minitreeOut_zd40_4j_weighted.root"
    TTreeName = "miniTree"

    for TFileName in TFileNameList:

        rootDict = readMinitreeToDictFromRootFile(TFileName,TTreeName)


        # in eihter case, presume the first two entries reconstruct the Z, the second and last pair each reconstruct respectively the Zd
        if "lepton1_pz" in rootDict.keys():
            fermionTuple = ('Refined4Jets_1of4','Refined4Jets_2of4','Refined4Jets_3of4','Refined4Jets_4of4','lepton1','lepton2')
        elif "Refined6Jets_6of6_pz" in rootDict.keys():
            fermionTuple = ("Refined6Jets_1of6","Refined6Jets_2of6","Refined6Jets_3of6","Refined6Jets_4of6","Refined6Jets_5of6","Refined6Jets_6of6")

        #for key in rootDict: del rootDict[key][2000-1:-1]
        
        print(TFileName)
        makeTMVAInputVariables(rootDict , fermionTuple )


        dictToTTree.dictToTTree(rootDict, TFileName = re.sub("weighted","TMVA",TFileName), TTreeName = TTreeName )

    #import itertools
    #AAA = [1,2,3,4]
    ## get all possible subsets of length 2 from AAA:
    #for x in itertools.combinations(AAA, 2): x


    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here