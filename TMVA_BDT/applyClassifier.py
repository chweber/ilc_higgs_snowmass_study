import ROOT
import array # for pass by reference
import re
import copy

def runBDT(inputFileName, signalOrBackground, BDTWeights, cutValue):

    #TMVAReader = ROOT.TMVA.Reader( "!Color:!Silent" )
    TMVAReader = ROOT.TMVA.Reader( "!Color:!Silent" )



    ff01_pt       = array.array('f',[0]) 
    ff01_M        = array.array('f',[0]) 
    ff01_E        = array.array('f',[0]) 
    ff01_CosTheta        = array.array('f',[0]) 
    ff23_pt       = array.array('f',[0]) 
    ff23_M        = array.array('f',[0]) 
    ff23_E        = array.array('f',[0]) 
    ff23_CosTheta        = array.array('f',[0]) 
    ff45_pt       = array.array('f',[0]) 
    ff45_M        = array.array('f',[0]) 
    ff45_E        = array.array('f',[0]) 
    ff45_CosTheta        = array.array('f',[0]) 
    dR_ff01_ff23  = array.array('f',[0]) 
    dR_ff01_ff45  = array.array('f',[0]) 
    dR_ff23_ff45  = array.array('f',[0]) 
    ffff2345_pt   = array.array('f',[0]) 
    ffff2345_M    = array.array('f',[0]) 
    ffff2345_E    = array.array('f',[0]) 

    nSiTracks     = array.array('f',[0]) 
    nElectrons    = array.array('f',[0]) 
    nMuons        = array.array('f',[0]) 
    nPFOs         = array.array('f',[0]) 

    weight        = array.array('f',[0]) 


    
    TMVAReader.AddVariable( "ff01_pt"   , ff01_pt        );
    TMVAReader.AddVariable( "ff01_M"    , ff01_M         );
    TMVAReader.AddVariable( "ff01_E"    , ff01_E         );
    TMVAReader.AddVariable( "ff01_CosTheta"    , ff01_CosTheta         );

    TMVAReader.AddVariable( "ff23_pt"   , ff23_pt        );
    TMVAReader.AddVariable( "ff23_M"    , ff23_M         );
    TMVAReader.AddVariable( "ff23_E"    , ff23_E         );
    TMVAReader.AddVariable( "ff23_CosTheta"    , ff23_CosTheta         );

    TMVAReader.AddVariable( "ff45_pt"   , ff45_pt        );
    TMVAReader.AddVariable( "ff45_M"    , ff45_M         );
    TMVAReader.AddVariable( "ff45_E"    , ff45_E         ); #  issue with 2l2j mZd60GeV sample and H ZZ background sample
    TMVAReader.AddVariable( "ff45_CosTheta"    , ff45_CosTheta         );

    TMVAReader.AddVariable( "dR_ff01_ff23", dR_ff01_ff23 );
    TMVAReader.AddVariable( "dR_ff01_ff45", dR_ff01_ff45 );
    TMVAReader.AddVariable( "dR_ff23_ff45", dR_ff23_ff45 );

    TMVAReader.AddVariable( "ffff2345_pt", ffff2345_pt   );
    TMVAReader.AddVariable( "ffff2345_M" , ffff2345_M    );
    TMVAReader.AddVariable( "ffff2345_E" , ffff2345_E    );

    TMVAReader.AddVariable( "nSiTracks" , nSiTracks    );    
    TMVAReader.AddVariable( "nElectrons" , nElectrons    );    
    TMVAReader.AddVariable( "nMuons" , nMuons    );    
    TMVAReader.AddVariable( "nPFOs" , nPFOs    );    

    TMVAReader.BookMVA("BDT",BDTWeights)



    inputFile = ROOT.TFile.Open( inputFileName )
    #inputFile = ROOT.TFile.Open( "minitreesV2_weighted/minitreeOut_zd40_4j_TMVA.root" )

    TTree = inputFile.Get("miniTree")

    TTree.SetBranchAddress( "ff01_pt"   , ff01_pt        )
    TTree.SetBranchAddress( "ff01_M"    , ff01_M         )
    TTree.SetBranchAddress( "ff01_E"    , ff01_E         )
    TTree.SetBranchAddress( "ff01_CosTheta",ff01_CosTheta )
    TTree.SetBranchAddress( "ff23_pt"   , ff23_pt        )
    TTree.SetBranchAddress( "ff23_M"    , ff23_M         )
    TTree.SetBranchAddress( "ff23_E"    , ff23_E         )
    TTree.SetBranchAddress( "ff23_CosTheta",ff23_CosTheta )
    TTree.SetBranchAddress( "ff45_pt"   , ff45_pt        )
    TTree.SetBranchAddress( "ff45_M"    , ff45_M         )
    TTree.SetBranchAddress( "ff45_E"    , ff45_E         )
    TTree.SetBranchAddress( "ff45_CosTheta",ff45_CosTheta )
    TTree.SetBranchAddress( "dR_ff01_ff23", dR_ff01_ff23 )
    TTree.SetBranchAddress( "dR_ff01_ff45", dR_ff01_ff45 )
    TTree.SetBranchAddress( "dR_ff23_ff45", dR_ff23_ff45 )
    TTree.SetBranchAddress( "ffff2345_pt", ffff2345_pt   )
    TTree.SetBranchAddress( "ffff2345_M" , ffff2345_M    )
    TTree.SetBranchAddress( "ffff2345_E" , ffff2345_E    )

    TTree.SetBranchAddress( "nSiTracks" , nSiTracks    )
    TTree.SetBranchAddress( "nElectrons" , nElectrons    )
    TTree.SetBranchAddress( "nMuons" , nMuons    )
    TTree.SetBranchAddress( "nPFOs" , nPFOs    )


    TTree.SetBranchAddress( "weight" , weight    );

    nEvents = TTree.GetEntries()

    #cutValue = -0.26

    nbin=20

    histBDT     = ROOT.TH1D( "MVA_BDT_"+signalOrBackground, "MVA_BDT_"+signalOrBackground, nbin, 1,-1 );
    histAverageMZd     = ROOT.TH1D( "averageMZd_"+signalOrBackground, "averageMZd_"+signalOrBackground, nbin, 0,100);
    histmZ1     = ROOT.TH1D( "mZ1_"+signalOrBackground, "mZ1_"+signalOrBackground, nbin, 0,100);
    histmZ2     = ROOT.TH1D( "mZ2_"+signalOrBackground, "mZ2_"+signalOrBackground, nbin, 0,100);
    histmZ3     = ROOT.TH1D( "mZ3_"+signalOrBackground, "mZ3_"+signalOrBackground, nbin, 0,100);
    histmH     = ROOT.TH1D(  "mH"+signalOrBackground,   "mH"+signalOrBackground, nbin, 90,160);

    for eventNr in range(0,nEvents):

        TTree.GetEntry(eventNr)

        BDTScore = TMVAReader.EvaluateMVA( "BDT" );

        passed = BDTScore >= cutValue
        #print((BDTScore,passed))

        if passed:
            histBDT.Fill(BDTScore,weight[0])
            histAverageMZd.Fill( (ff23_M[0]+ff45_M[0])/2  ,weight[0])
            histmZ1.Fill( ff01_M[0]  ,weight[0])
            histmZ2.Fill( ff23_M[0]  ,weight[0])
            histmZ3.Fill( ff45_M[0]  ,weight[0])
            histmH.Fill(  ffff2345_M[0]  ,weight[0])


            #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here


    histAverageMZd.Scale(1.0)
    outputHistList = [copy.deepcopy(histBDT),copy.deepcopy(histAverageMZd),copy.deepcopy(histmZ1), copy.deepcopy(histmZ2), copy.deepcopy(histmZ3), copy.deepcopy(histmH)]

    for hist in outputHistList: hist.Scale(2000.) # scale to 2000 inf femto barn



    return outputHistList

# TMVA Classification example: https://root.cern.ch/doc/v608/TMVAClassificationApplication_8C.html
if __name__ == '__main__':


    ### 4j final states ###
    #BDTWeights     = "dataset_BDTs_4j_mZd20GeV/weights/TMVAClassification_BDT.weights.xml"
    #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_20GeV_trained_alt.root"
    #BDTCut = -0.01

    #BDTWeights     = "dataset_BDTs_4j_mZd40GeV/weights/TMVAClassification_BDT.weights.xml"
    #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_40GeV_trained.root"
    #BDTCut = -0.12

    #BDTWeights     = "dataset_BDTs_4j_mZd60GeV/weights/TMVAClassification_BDT.weights.xml"
    #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_60GeV_trained.root"
    #BDTCut = -0.1

    #BDTWeights     = "dataset_BDTs_4j_mZd204060GeV/weights/TMVAClassification_BDT.weights.xml"
    #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_204060GeV_trained.root"
    #BDTCut = -0.02


    ### Higgs + ZZ ###
    BDTWeights     = "dataset_BDTs_4j_mZd20GeV_ZZH/weights/TMVAClassification_BDT.weights.xml"
    outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_20GeV_trained_HZZ.root"
    BDTCut =         -0.09

    #BDTWeights     = "dataset_BDTs_4j_mZd40GeV_ZZH/weights/TMVAClassification_BDT.weights.xml"
    #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_40GeV_trained_HZZ.root"
    #BDTCut =         -0.08

    #BDTWeights     = "dataset_BDTs_4j_mZd60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml"
    #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_60GeV_trained_HZZ.root"
    #BDTCut =         -0.09

    #BDTWeights     = "dataset_BDTs_4j_mZd20_40_60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml"
    #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_204060GeV_trained_HZZ.root"
    #BDTCut =         -0.04

    inputList = [
    ("dataset_BDTs_4j_mZd20GeV_ZZH/weights/TMVAClassification_BDT.weights.xml" ,"minitreesV3_post_BDT_hists/BDTHistOut_4j_20GeV_trained_HZZ.root" , -0.09) ,
    ("dataset_BDTs_4j_mZd40GeV_ZZH/weights/TMVAClassification_BDT.weights.xml" ,"minitreesV3_post_BDT_hists/BDTHistOut_4j_40GeV_trained_HZZ.root" ,-0.08) ,
    ("dataset_BDTs_4j_mZd60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml" , "minitreesV3_post_BDT_hists/BDTHistOut_4j_60GeV_trained_HZZ.root",-0.09) ,
    ("dataset_BDTs_4j_mZd20_40_60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml", "minitreesV3_post_BDT_hists/BDTHistOut_4j_204060GeV_trained_HZZ.root", -0.04) ,
    ("dataset_BDTs_2l2j_mZd20GeV_ZZH/weights/TMVAClassification_BDT.weights.xml" , "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_20GeV_trained_HZZ.root", -0.18),
    ("dataset_BDTs_2l2j_mZd40GeV_ZZH/weights/TMVAClassification_BDT.weights.xml" , "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_40GeV_trained_HZZ.root", -0.04),
    ("dataset_BDTs_2l2j_mZd20_40_60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml" , "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_204060GeV_trained_HZZ.root", +0.07) ]


    inputList = [ ("dataset_BDTs_2l2j_mZd60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml" , "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_60GeV_trained_HZZ.root", +0.07)]

    inputList = [
    ("dataset_BDTs_4j_mZd20_40_60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml", "minitreesV3_post_BDT_hists/BDTHistOut_4j_204060GeV_trained_HZZ.root", -0.04) ,
    ("dataset_BDTs_2l2j_mZd20_40_60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml" , "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_204060GeV_trained_HZZ.root", +0.07) ]


    for BDTWeights, outputFileName , BDTCut in inputList :


        ### UOregon + Higgs, eLpR only ###
        #BDTWeights     = "dataset_eLpR_BDTs_4j_mZd20_40_60GeV/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_4j_204060GeV_trained_UOregon2f1heLpR.root"
        #BDTCut = -0.03



        ### 2l2j final states ###
        #BDTWeights     = "dataset_BDTs_2l2j_mZd20GeV/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_20GeV_trained.root"
        #BDTCut = +0.11

        #BDTWeights     = "dataset_BDTs_2l2j_mZd40GeV/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_40GeV_trained.root"
        #BDTCut = +0.03

        #BDTWeights     = "dataset_BDTs_2l2j_mZd60GeV/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_60GeV_trained.root"
        #BDTCut = +0.07

        #BDTWeights     = "dataset_BDTs_2l2j_mZd204060GeV/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_204060GeV_trained.root"
        #BDTCut = +0.15

        #### Higgs + ZZ ###
        #BDTWeights     = "dataset_BDTs_2l2j_mZd20GeV_ZZH/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_20GeV_trained_HZZ.root"
        #BDTCut =         -0.18

        #BDTWeights     = "dataset_BDTs_2l2j_mZd40GeV_ZZH/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_40GeV_trained_HZZ.root"
        #BDTCut =         -0.04

        #BDTWeights     = "dataset_BDTs_2l2j_mZd60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_60GeV_trained_HZZ.root"
        #BDTCut =         +0.07

        #BDTWeights     = "dataset_BDTs_2l2j_mZd20_40_60GeV_ZZH/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_204060GeV_trained_HZZ.root"
        #BDTCut =         +0.07



        ### UOregon + Higgs, eLpR only ###
        #BDTWeights     = "dataset_eLpR_BDTs_2l2j_mZd20_40_60GeV/weights/TMVAClassification_BDT.weights.xml"
        #outputFileName = "minitreesV3_post_BDT_hists/BDTHistOut_2l2j_204060GeV_trained_UOregon2f1heLpR.root"
        #BDTCut = +0.23





        #canvas = ROOT.TCanvas()
        #canvas.Draw()
        #histBDT.Draw()

        outputHists = []

        if "2l2j" in BDTWeights: finalState = "2l2j"
        elif "4j" in BDTWeights: finalState = "4j"
       
        outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_UOregon2f1h_%s_TMVA.root"     %finalState, "background" , BDTWeights , BDTCut))
        outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_2f1hrecoeLpR_%s_TMVA.root"    %finalState, "backgroundHiggsOnly" , BDTWeights , BDTCut))
        outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_2f1h4fZZeLpR_%s_TMVA.root"    %finalState, "H_ZZ_Background" , BDTWeights , BDTCut))
        outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_UOregon2f1heLpR_%s_TMVA.root" %finalState, "background_eLpR" , BDTWeights , BDTCut))

        outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_zd20_%s_TMVA.root"     %finalState, "signal_20GeV" , BDTWeights , BDTCut))
        outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_zd40_%s_TMVA.root"     %finalState, "signal_40GeV" , BDTWeights , BDTCut))
        outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_zd60_%s_TMVA.root"     %finalState, "signal_60GeV" , BDTWeights , BDTCut))
        outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_zd204060_%s_TMVA.root" %finalState, "signal_204060GeV" , BDTWeights , BDTCut))

        #outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_UOregon2f1h_2l2j_TMVA.root" , "background" , BDTWeights , BDTCut))
        #outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_zd20_2l2j_TMVA.root" , "signal_20GeV" , BDTWeights , BDTCut))
        #outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_zd40_2l2j_TMVA.root" , "signal_40GeV" , BDTWeights , BDTCut))
        #outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_zd60_2l2j_TMVA.root" , "signal_60GeV" , BDTWeights , BDTCut))
        #outputHists.extend( runBDT( "minitreesV3_TMVA/minitreeOut_zd204060_2l2j_TMVA.root" , "signal_204060GeV" , BDTWeights , BDTCut))


        #outputFileName = re.sub("minitreeOut","BDTHistOut",inputFileName)

        outputRootFile = ROOT.TFile(outputFileName,"RECREATE")

        for hist in outputHists: hist.Write()

        outputRootFile.Close()

    print("All done!")

    #print( outputHists[3].Integral() )
    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here
    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here


