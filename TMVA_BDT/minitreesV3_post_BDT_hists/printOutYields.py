import ROOT
import collections

templatePaths = collections.defaultdict(lambda: collections.defaultdict(dict)) 

for mass in [20,40,60]:

    templatePaths["finalSate_4j"]["signal"] = "averageMZd_signal_%iGeV" %mass
    templatePaths["finalSate_4j"]["background"] = "averageMZd_H_ZZ_Background"
    templatePaths["finalSate_4j"]["TFile"] = "BDTHistOut_4j_%iGeV_trained_HZZ.root" %mass
    #templatePaths["finalSate_4j"]["TFile"] = "BDTHistOut_4j_204060GeV_trained_HZZ.root" 


    templatePaths["finalSate_2l2j"]["signal"] = "averageMZd_signal_%iGeV" %mass
    templatePaths["finalSate_2l2j"]["background"] = "averageMZd_H_ZZ_Background"
    templatePaths["finalSate_2l2j"]["TFile"] = "BDTHistOut_2l2j_%iGeV_trained_HZZ.root" %mass
    #templatePaths["finalSate_2l2j"]["TFile"] = "BDTHistOut_2l2j_204060GeV_trained_HZZ.root"


    for finalState in  templatePaths:

        file = ROOT.TFile(templatePaths[finalState]["TFile"],"OPEN")


        for sigOrBackg in [ "signal","background"]: #


            hist = file.Get(templatePaths[finalState][sigOrBackg])
    
            print( (mass, templatePaths[finalState]["TFile"],finalState, templatePaths[finalState][sigOrBackg], hist.Integral() ))



#import pdb; pdb.set_trace() # import the debugger and instruct it to stop here