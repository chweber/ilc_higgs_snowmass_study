import re
import os
import time 
import subprocess
import datetime

import difflib

def makeSubmitScript( shellScriptName):

    submitScriptName = re.sub(".sh", ".sub", shellScriptName) # replace '.sh' with '.sub'
    with open(submitScriptName, "w") as submitWrite:
        submitWrite.write("universe                = vanilla\n" )
        submitWrite.write("executable              = "+shellScriptName+"\n" )
        submitWrite.write("# Get the environment from the submission host?\n" )
        submitWrite.write("#arguments               = $Fnx(filename)\n" )
        submitWrite.write("output                  = $(ClusterId).$(ProcId).out\n" )
        submitWrite.write("error                   = $(ClusterId).$(ProcId).err\n" )
        submitWrite.write("log                     = $(ClusterID).log\n" )
        #submitWrite.write("request_memory          = 4 GB\n" )
        submitWrite.write("getenv                  = True\n" )
        submitWrite.write("#should_transfer_files   = YES\n" )
        submitWrite.write("#when_to_transfer_output = ON_EXIT\n" )
        submitWrite.write("accounting_group = group_atlas.tier3.yale\n" )
        #submitWrite.write("accounting_group = group_atlas.tier3.yale.long\n" )
        submitWrite.write("Queue 1\n" )
        #submitWrite.write("#queue filename matching (/direct/usatlas+u/chweber/ZdZd_area/Pull-rankingTool/condor/mZd15_combined_NormalMeasurement_model/*.sh)\n" )

    os.chmod(submitScriptName, 0o755)


    return submitScriptName


def chunksOfList(aList, chunkSize):
    """Yield successive n-sized chunks from lst."""
    for index in range(0, len(aList), chunkSize):
        yield aList[index:index+ chunkSize]


def get_substring_overlap_info(string1, strin2):
    s = difflib.SequenceMatcher(None, string1, strin2)
    pos_a, pos_b, size = s.find_longest_match(0, len(string1), 0, len(strin2)) 
    return pos_a, pos_b, size


def get_string_with_largest_overlap(referenceString, stringList, commonPosition = None):

    bestMatchSize = -1
    bestMatchString = None

    for string in stringList:

        pos_a, pos_b, size = get_substring_overlap_info(referenceString, string)

        improvedSizeMatch = size > bestMatchSize

        if commonPosition is None: commonPostionMatch = True # pass automaticaly if commonPosition is unspecified
        else: commonPostionMatch = commonPosition == pos_a and commonPosition == pos_b

        if improvedSizeMatch and commonPostionMatch:
            bestMatchSize = size
            bestMatchString = string

    return bestMatchString

def make_file_metaDataFile_dict(files, metaDataFiles):

    file_metaDataFile_dict = {}

    for file in files:

        closestMatch = difflib.get_close_matches( file  , metaDataFiles, n=1, cutoff =0.3 )[0]

        closestMatchCrossCheck = get_string_with_largest_overlap(file, metaDataFiles, commonPosition = None)

        if closestMatch == closestMatchCrossCheck:   file_metaDataFile_dict[file] = closestMatch

    return file_metaDataFile_dict


def make_file_metaDataFile_dict_fast(files, metaDataFiles):

    files.sort()
    metaDataFiles.sort()

    file_metaDataFile_dict = {}

    for file, metaDataFile in zip(files, metaDataFiles):

        fileNameStart = re.search(".+_\d+\.",file).group()
        metaDataNameStart = re.search(".+_\d+\.",metaDataFile).group()

        assert fileNameStart == metaDataNameStart

        file_metaDataFile_dict[file] = metaDataFile

    return file_metaDataFile_dict

#all_SM_background_+80e-_-30e+_100.stdhep.delphes_card_DSiDi.tcl.root.txt
#all_SM_background_+80e-_-30e+_100.stdhep.sim.v02-00-02.SiD_o2_v03_reco.slcio

def make_metaData_submitString(MCSampleChunks, metaDataFileChunk, metaDataDir):

    MCSampleChunks_With_Metadata = []

    for MCSample in MCSampleChunks: 
        medataDataFile = metaDataFile_dict.get(MCSample)
        if medataDataFile is not None: 
            MCSampleChunks_With_Metadata.append(MCSample)
            metaDataFileChunk.append( os.path.join(metaDataDir, medataDataFile) )

    return MCSampleChunks_With_Metadata, metaDataFileChunk

if __name__ == '__main__':


    nMCSamplesPerSubmission = 5

    timeTag = datetime.datetime.now().strftime("%Y%m%d%H%M")

    #MCSampleDirList = ["/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/2f1h_inv_reco",
    #                   "/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/background_reco",
    #                   "/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/2f1h_reco",
    #                   "/usatlas/u/snyder/usatlasdata/ilc/data/reco"] # signals
    ##MCSampleDirList = [ "/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/background_reco" ]
    ##MCSampleDirList = [ "/usatlas/u/snyder/usatlasdata/ilc/data/reco"]
    ##outputParentDir = "cutflowOut_HInvjj_"+timeTag
    #outputParentDir = "cutflowOut_Minitree_"+timeTag
    #MCSampleDirList = ["/usatlas/u/chweber/usatlasdata/ILC/SnowmassSamples/Whizard2.8.5/higgs",
    #                   "/usatlas/u/chweber/usatlasdata/ILC/SnowmassSamples/Whizard2.8.5/4f",
    #                   "/usatlas/u/chweber/usatlasdata/ILC/SnowmassSamples/Whizard2.8.5/2f"]
    ##outputParentDir = "cutflowOut_HInvjj_noSiTracksCut_SnowmassSamples_"+timeTag
    #MCSampleDirList = ["/usatlas/u/chweber/usatlasdata/UOregonBackgrounds_1X_reco"] # Reconstructed UOregon Background samples
    #MCSampleDirList = ["/usatlas/u/snyder/usatlasdata/ilc/data/reco/zd40_jjjj_500"]
    ##MCSampleDirList = ["/usatlas/u/snyder/usatlasdata/ilc/data/reco/zd40_lljj_500"]
    #MCSampleDirList = ["/usatlas/u/snyder/usatlasdata/ilc/data/reco"] # signals
    #MCSampleDirList = ["/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/2f1h_reco"]
    #MCSampleDirList = ["/usatlas/u/chweber/usatlasdata/UOregon_montecarlo2021_ilc250_whizard_4f_ZZ"]


    #ZdZd_4j_dir = ["/usatlas/u/snyder/usatlasdata/ilc/data/reco/zd40_jjjj_500"] # ZdZd signal, 4j final state, mZd = 40GeV
    ZdZd_4j_dir_20 = ["/usatlas/u/chweber/usatlasdata/ILC/ZdZdSignal/zd20_jjjj_20000_reco"]
    ZdZd_4j_dir_40 = ["/usatlas/u/chweber/usatlasdata/ILC/ZdZdSignal/zd40_jjjj_20000_reco"]
    ZdZd_4j_dir_60 = ["/usatlas/u/chweber/usatlasdata/ILC/ZdZdSignal/zd60_jjjj_20000_reco"]

    #ZdZd_2l2j_dir = ["/usatlas/u/snyder/usatlasdata/ilc/data/reco/zd40_lljj_500"] # ZdZd signal, 2l2j final state, mZd = 40GeV
    ZdZd_2l2j_dir_20 = ["/usatlas/u/chweber/usatlasdata/ILC/ZdZdSignal/zd20_lljj_20000_reco"]
    ZdZd_2l2j_dir_40 = ["/usatlas/u/chweber/usatlasdata/ILC/ZdZdSignal/zd40_lljj_20000_reco"]
    ZdZd_2l2j_dir_60 = ["/usatlas/u/chweber/usatlasdata/ILC/ZdZdSignal/zd60_lljj_20000_reco"]

    generic_noHiggs_dir = ["/gpfs/mnt/atlasgpfs01/usatlas/data/chweber/ILC/UOregonSamples/UOregonBackgrounds_1X_reco"] #generic Background (no Higgs) 

    generic_noHiggs_eRpL_dir = ["/gpfs/mnt/atlasgpfs01/usatlas/data/chweber/ILC/UOregonSamples/UOregonBackgrounds_1X_reco_eRpL"] #generic Background (no Higgs) 
    generic_noHiggs_eLpR_dir = ["/gpfs/mnt/atlasgpfs01/usatlas/data/chweber/ILC/UOregonSamples/UOregonBackgrounds_1X_reco_eLpR"] #generic Background (no Higgs) 
    
    higgsInclusice_eRpL_dir = ["/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/2f1h_reco_eRpL"] #Higgs Inclusive
    higgsInclusice_eLpR_dir = ["/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/2f1h_reco_eLpR"] #Higgs Inclusive

    nonResonantZZ_dir = ["/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/4f_ZZrecoeLpR"] # non-resonant ZZ

    counter = 0

    

    for MCSampleDirList in [nonResonantZZ_dir ]:
    #for MCSampleDirList in [generic_noHiggs_eRpL_dir, generic_noHiggs_eLpR_dir ]:
    #for MCSampleDirList in [higgsInclusice_eRpL_dir, higgsInclusice_eLpR_dir ]:#
    #for MCSampleDirList in [ZdZd_4j_dir_20,   ZdZd_4j_dir_40,   ZdZd_4j_dir_60   ]:
    #for MCSampleDirList in [ZdZd_2l2j_dir_20, ZdZd_2l2j_dir_40, ZdZd_2l2j_dir_60 ]:


        for cutflowType in [ "4j_finalState_v2","2l2j_finalState_v2"]:


            inputFileFolder = MCSampleDirList[0].split("/")[-1]
            outputParentDir = "minitreeOut_%s_%s" %( inputFileFolder, timeTag)

            if not os.path.isdir(outputParentDir): os.mkdir(outputParentDir)

            for MCSampleDir in MCSampleDirList:

                physicsProcess = os.path.basename(MCSampleDir)
                MCSampleList = [file for file in os.listdir(MCSampleDir) if os.path.isfile( os.path.join(MCSampleDir,file)) and  file.endswith(".slcio")]

                MCSampleList.sort()

                if "metadata" in os.listdir(MCSampleDir):

                    metaDataDir = os.path.join(MCSampleDir,"metadata")

                    metaDataFiles = os.listdir(metaDataDir)
                    metaDataFiles.sort()

                    #metaDataFile_dict = make_file_metaDataFile_dict(MCSampleList, metaDataFiles)
                    metaDataFile_dict = make_file_metaDataFile_dict_fast(MCSampleList, metaDataFiles)
                    
                else: metaDataFile_dict = False



                
                for MCSampleChunks in chunksOfList(MCSampleList, nMCSamplesPerSubmission):
                    counter +=1

                    metaDataFileChunk = []

                    if metaDataFile_dict: MCSampleChunks , metaDataFileChunk = make_metaData_submitString(MCSampleChunks, metaDataFileChunk, metaDataDir)

                    if len(MCSampleChunks) < 1: continue


                    #outputFileName = "cutflowOut_%04d_%s_%s_.root" %(counter, physicsProcess, timeTag)

                    #import pdb; pdb.set_trace()
                    baseFilename = os.path.basename(MCSampleChunks[0])
                    outputFileName = re.sub(".slcio", "_%s_MiniTree.root" %(cutflowType), baseFilename)


                    MCSampleChunks = [ os.path.join(MCSampleDir,  MCSample) for MCSample in MCSampleChunks ] # make sure we have the full path to the MC samples


                    #submitLine = "python HInv.py --input %s --physicsProcess %s --outputFileName %s --nEventsToProcess -1 " %(" ".join(MCSampleChunks), physicsProcess, os.path.join(outputParentDir,outputFileName) )
                    submitLine = "python HMinitreeMaker.py --input %s --physicsProcess %s --outputFileName %s --nEventsToProcess -1 --cutflowType %s " %(" ".join(MCSampleChunks), physicsProcess, os.path.join(outputParentDir,outputFileName), cutflowType )

                    # add in metadata files
                    if metaDataFile_dict: submitLine += " --metaDataFiles %s" %(" ".join(metaDataFileChunk))


                    shellScript = re.sub(".slcio", "_%04d.sh" %(counter), baseFilename)
                    #shellScript = re.sub(".root", ".sh", outputFileName)

                    #import pdb; pdb.set_trace()

                    with open(shellScript, "w") as shellWrite:
                        shellWrite.write('#!/bin/bash\n')
                        shellWrite.write('%s\n' %submitLine)
                        #b.write(gangacmd)

                    os.chmod(shellScript, 0o755)

                    submitScriptName = makeSubmitScript( shellScript)

                    #import pdb; pdb.set_trace()

                    os.system( "condor_submit " + submitScriptName)

                    #time.sleep(1)
                    #import pdb; pdb.set_trace()

    print("All submitted!")


    #import pdb; pdb.set_trace()