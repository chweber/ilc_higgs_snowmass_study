import ROOT
import argparse # to parse command line options
import os
import collections
import re
import copy


# import sys and os.path to be able to import things from the parent directory
import sys 
from os import path
sys.path.append( os.path.join( path.dirname( path.abspath(__file__) )  , "functions") )

import parseMetadata as parseMetadata
import dictToTTree as dictToTTree 


def makeProcessIDTallyDict(metadataFileList):
    # tally up the process IDs from the UOregon metadata text files
    # i.e. the ones that match event number for a given file to the event ID
    # output a dict that is like 
    # processIdTallyDict[ idNumber ] = #absolulte frequency of that id number occuring within the metadata files

    # if we don't get a meta data file list, we can build it form a directory
    if isinstance(metadataFileList, str):

        if not os.path.isdir(metadataFileList): return None
        metadataFileList = [  os.path.join(metadataFileList, file) for file in os.listdir(metadataFileList)]

    # processIdTallyDict[ processID] = number of occurances among all the meta data files in todal
    processIdTallyDict = collections.defaultdict(int) 

    for metadataFile in metadataFileList:
        processIDList = parseMetadata.get_eventID_list(metadataFile)
        for processID in processIDList: processIdTallyDict[processID] += 1

    return processIdTallyDict


def tallyProcessIDsInDict(rootDict, processIDVar = 'process_id'):

    tallyDict = collections.defaultdict(int) 

    for processID in rootDict[processIDVar]:    tallyDict[processID] += 1

    return tallyDict

def makeAcceptanceTimesEfficiencyDict(postCutflowTallyDict, fullEnsambleTallyDict): 

    acceptanceEfficiencyDict = {} 

    for processID in postCutflowTallyDict:
        acceptanceEfficiencyDict[processID] = float(postCutflowTallyDict[processID])/float(fullEnsambleTallyDict[processID])

    return acceptanceEfficiencyDict


def TTreeToDict(TTree):

    outputDict = collections.defaultdict(list)  # store here information that we will output as a TTree

    #listOfBranches = [ branch for branch in TTree.GetListOfBranches()]
    listOfBrancheNames = [ branch.GetName() for branch in TTree.GetListOfBranches()]

    for event in TTree: 
        for branch in listOfBrancheNames:
            outputDict[branch].append( eval("event."+branch) )
    
    return outputDict


def readMinitreeToDictFromRootFile(rootFileLocation, miniTreeName ,additionalObjectsToRead = None):

    additonalOutputs = {}

    ROOTFile = ROOT.TFile(rootFileLocation,"OPEN")
    TTree = ROOTFile.Get(miniTreeName)
    TTreeName = TTree.GetName()
    rootDict = TTreeToDict(TTree)

    if additionalObjectsToRead is not None:
        for objectName in additionalObjectsToRead:
            additonalOutputs[objectName] = copy.copy( ROOTFile.Get(objectName) )

    ROOTFile.Close()

    if additionalObjectsToRead is not None: return rootDict, additonalOutputs

    return rootDict


def reweightAndWriteRootFile(ROOTFileLocation,  crossSectionOrDict = None):

    reweightNonGenericBackground     = False
    reweightGenericUOregonBackground = False
    TTreeName = "miniTree"

    # infer what we need to do from the type that is crossSectionOrDict
    if isinstance(crossSectionOrDict, int) or isinstance(crossSectionOrDict, float):
        crossSection = crossSectionOrDict
        reweightNonGenericBackground = True

    elif  isinstance(crossSectionOrDict, dict):
        processIDTallyDict = crossSectionOrDict
        reweightGenericUOregonBackground = True

    if reweightGenericUOregonBackground :

        rootDict = readMinitreeToDictFromRootFile(ROOTFileLocation, TTreeName)

        postCutflowProcessTallyDict =  tallyProcessIDsInDict(rootDict)
        acceptanceEfficiencyDict = makeAcceptanceTimesEfficiencyDict(postCutflowProcessTallyDict, processIDTallyDict)

        #crossSectionDict = parseMetadata.make_processID_XS_dict()
        crossSectionDict = parseMetadata.make_processID_XS_dict_from_json()

        rootDict.pop('weight',None)
        #for processID in rootDict['process_id']:        rootDict["weight"].append(crossSectionDict[processID]*acceptanceEfficiencyDict[processID])
        for processID in rootDict['process_id']:        rootDict["weight"].append(crossSectionDict[processID]/processIDTallyDict[processID])


        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

        dictToTTree.dictToTTree(rootDict, TFileName = re.sub(".root","_weighted.root",ROOTFileLocation), TTreeName = TTreeName )


    elif reweightNonGenericBackground: 

        rootDict, additionalOutputDict = readMinitreeToDictFromRootFile(ROOTFileLocation, TTreeName, additionalObjectsToRead = ["acceptanceHist;1"])
        acceptanceHist = additionalOutputDict["acceptanceHist;1"]

        rootDict.pop('weight',None)
        for processID in rootDict['process_id']:        rootDict["weight"].append( crossSection /acceptanceHist.GetBinContent(1) ) 

        print(rootDict["weight"][0])

        dictToTTree.dictToTTree(rootDict, TFileName = re.sub(".root","_weighted.root",ROOTFileLocation), TTreeName = TTreeName , listOfOtherRootObjectsToWriteOut =[acceptanceHist] )

    return None


def haddFolders(folder, finalStates = ["2l2j", "4j"]):

    newFileNameList = []

    for finalState in finalStates:

        if   "jjjj" in folder and finalState != "4j":   continue
        elif "lljj" in folder and finalState != "2l2j": continue


        fileNameComponents = folder.split("_")[0:2]; fileNameComponents.append(finalState)
        fileBaseName = "_".join(folder.split("_")[0:2]) 
        newFileName = "_".join(fileNameComponents) + ".root"

        haddString = "hadd -f " + newFileName +" " + os.path.join(folder , "*%s*.root" %finalState)
        os.system( haddString )

        newFileNameList.append(newFileName)
        #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here

    return newFileNameList

def getReweightedRootFilesFromCondorOutputs(folderAndXS):
    # list of tuple, that is like (path to HMinitreeMater output dir, numerical Crosss section or processIDTallyDict)

    for folder, XS in folderAndXS:

        haddedFiles = haddFolders(folder)

        for file in haddedFiles: reweightAndWriteRootFile(file, crossSectionOrDict = XS)

    return None

def initilizeArgParser( parser = argparse.ArgumentParser() ):

    parser.add_argument("--input", type=str, nargs='*', help="set of paths to files which want to process")

    parser.add_argument("--metaDataFiles", type=str, nargs='*', 
        help="set of paths to metadata files that contain the event IDs for the events in the input files. \
              Use to determine the background event weights.")

    parser.add_argument( "--physicsProcess", type=str, default="process",
        help = "Name of the main physics process that we are processing, e.g. ZH->jj+invisible, or background, etc"  )  

    parser.add_argument( "--nEventsToProcess", type=int, default=-1,
        help = "Number of events we want to process. --nEventsToProcess -1 implies processing of all events"  )  

    parser.add_argument( "--outputFileName", type=str, default="output_HInvCutflow.root",
        help = "Name of the main physics process that we are processing, e.g. ZH->jj+invisible, or background, etc"  )  

    return parser


if __name__ == '__main__':

    # All cross section in femto barn
    # cross sections from Metadata / UOregon background samples are in fb, need all other ones too

    # 
    # Ee -> ZH cross section ~280 fb
    # 
    # ATLAS gg->H->ZdZd->4l limits: ~ .1 fb
    # Higgs ggF cross section 48.58 pb = 48580. fb
    # 
    # => BR( H->ZdZd->4l )  = .1 / 48580. = 2E-6 fb
    # 
    # ZdZd Brancing ratios see https://arxiv.org/pdf/1312.4992.pdf, figure 13 a on page 45
    # 
    # BR(Zd -> ee or mumu) = .12 to 0.14, so go with 0.13
    # This means BR(Zd -> ll) = 0.26
    # BR(Zd -> bb + cc + light flavor) =  0.06 + 0.20 + 0.32 = 0.58 (@ mZd = 40 GeV)
    # 
    # => BR(ZdZd -> 4j) / BR(ZdZd -> 4l) = 0.58**2 / 0.26**2 = 5.0
    # => BR(ZdZd -> 2l2j) / BR(ZdZd -> 4l) = 0.58 * 0.26 / 0.26**2 = 2.2
    # 
    # => BR( H->ZdZd->4j ) =2E-6 *5.0 = 10E-6
    # 
    # => BR( H->ZdZd->2l2j ) =2E-6 *2.2 = 4.4e-06
    # 
    # 
    # BR(Z -> jj) = 0.69
    # 
    # => XS( ee -> ZH -> jj ZdZd -> jj 4j )   = 280 fb * 0.69 * 10E-6   =  2E-3   fb
    # => XS( ee -> ZH -> jj ZdZd -> jj 2l2j ) = 280 fb * 0.69 * 4.4e-06 =  8.5E-4 fb


    # ee -> ZH : 280 fb
    # Z -> jj: 0.69 BR
    # ee -> ZH -> Z ZdZd -> 280 fb * 2E-6  = 0.56E-3 fb
    # ee -> ZH -> Z ZdZd -> 280 fb * 2E-6 * 0.69**2 = 0.27E-3 fb

    #dirAndCrossSectionList = [("minitreeOut_zd40_lljj_500_202202101735", 1.)]
    #dirAndCrossSectionList = [("minitreeOut_UOregonBackgrounds_1X_reco_202201252012",makeProcessIDTallyDict("/usatlas/u/chweber/usatlasdata/UOregonBackgrounds_1X_reco/metadata"))    ]

    #dirAndCrossSectionList = [
    #("minitreeOut_2f1h_reco_202203100037", 262.)]
    #("minitreeOut_zd20_lljj_20000_reco_202203081922", 1.),
    #("minitreeOut_zd40_lljj_20000_reco_202203081922", 1.),
    #("minitreeOut_zd60_lljj_20000_reco_202203081922", 1.),
    #("minitreeOut_zd20_jjjj_20000_reco_202203081916", 1.),
    #("minitreeOut_zd40_jjjj_20000_reco_202203081916", 1.),
    #("minitreeOut_zd60_jjjj_20000_reco_202203081916", 1.) ]
    #("minitreeOut_UOregonZZ_montecarlo2021_ilc250_whizard_4f_ZZ_simulated_reco_202202101735", 1. ),
    #("minitreeOut_UOregonBackgrounds_1X_reco_202201252012",makeProcessIDTallyDict("/usatlas/u/chweber/usatlasdata/UOregonBackgrounds_1X_reco/metadata"))    ]


    #dirAndCrossSectionList = [("minitreeOut_UOregonBackgrounds_1X_reco_202201252012",makeProcessIDTallyDict("/usatlas/u/chweber/usatlasdata/UOregonBackgrounds_1X_reco/metadata"))    ]


    #dirAndCrossSectionList = [
    #("minitreeOut_2f1hrecoeRpL_202203120224",280.),
    #("minitreeOut_2f1hrecoeLpR_202203120224",211.),
    #("minitreeOut_4fZZrecoeLpR_202203130100",827.)
    #("minitreeOut_zd20_lljj_20000_reco_202203120222",1.),
    #("minitreeOut_zd40_lljj_20000_reco_202203120222",1.),
    #("minitreeOut_zd60_lljj_20000_reco_202203120222",1.),
    #("minitreeOut_zd20_jjjj_20000_reco_202203120222",1.),
    #("minitreeOut_zd40_jjjj_20000_reco_202203120222",1.),
    #("minitreeOut_zd60_jjjj_20000_reco_202203120222",1.),
    #("minitreeOut_UOregonBackgrounds1XrecoeRpL_202203122137", makeProcessIDTallyDict("/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/UOregonBackgrounds_1X_reco_eRpL/metadata/")),
    #("minitreeOut_UOregonBackgrounds1XrecoeLpR_202203122137", makeProcessIDTallyDict("/usatlas/u/chweber/usatlasdata/ILC/UOregonSamples/UOregonBackgrounds_1X_reco_eLpR/metadata/"))]


    dirAndCrossSectionList = [
    ("minitreeOut_zd20_lljj_20000_reco_202203120222",1.),
    ("minitreeOut_zd40_lljj_20000_reco_202203120222",1.),
    ("minitreeOut_zd60_lljj_20000_reco_202203120222",1.),
    ("minitreeOut_zd20_jjjj_20000_reco_202203120222",1.),
    ("minitreeOut_zd40_jjjj_20000_reco_202203120222",1.),
    ("minitreeOut_zd60_jjjj_20000_reco_202203120222",1.)]

    getReweightedRootFilesFromCondorOutputs(dirAndCrossSectionList)


    #import pdb; pdb.set_trace() # import the debugger and instruct it to stop here